
;Make CUCM XML for AU3
#include<File.au3>
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>


_XML_GUI()

Func _XML_GUI()

	#Region ### START Koda GUI section ### Form=
	$Form1 = GUICreate("Form1", 634, 438, 369, 244)
	$Edit1 = GUICtrlCreateEdit("", 8, 8, 617, 377)
	GUICtrlSetData(-1, "Edit1")
	$Button_Transform = GUICtrlCreateButton("Transform", 128, 400, 75, 25)
	$Button_Cancel = GUICtrlCreateButton("Cancel", 408, 400, 75, 25)
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

	While 1
		$nMsg = GUIGetMsg()
		Switch $nMsg
			Case $GUI_EVENT_CLOSE
				Exit

		EndSwitch
	WEnd

EndFunc

Func _MakeCUCMXMLforAU3()
	Local	$rFile, $wFile, $fCount, $oLine, $mLine
	$fCount = _FileCountLines($rFile)
	FileDelete($wFile)
	For $a = 1 To $fCount
		$oLine = FileReadLine($rFile, $a)
		$mLine = StringReplace($oLine,"<", "'<", 1)
		If $a = $fCount Then
			$mLine = StringReplace($mLine,">", ">'", -1)
		Else
			$mLine = StringReplace($mLine,">", ">'&@CR& _", -1)
		EndIf
		FileWriteLine($wFile, $mLine)
	Next
EndFunc
