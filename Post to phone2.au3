
#include <WinHTTP.au3>
;~ #include ".\Base64v2.au3"

Global	$oMyError = ObjEvent("AutoIt.Error", "_COMError") ; Install a custom error handler

$UCM = 'Lab'

If $UCM = 7 Then
	$ip = '' ;UCM7
	$Password = '' ;UCM7
ElseIf $UCM = 10 Then
	$ip = '' ;UCM10
	$Password = ''
Elseif $UCM = 'LAB' Then
	$ip = '' ;UCM10
	$Password = ''
EndIf

$G_P_Type = 'POST'
$UserID = '...'
$Data2  = 	'XML=<CiscoIPPhoneExecute>' & _
			'<ExecuteItem Priority="0"URL="Key:Settings"/>'& _
			'</CiscoIPPhoneExecute>'
$CommandType = 'CGI/Execute'

_Put()

;~ $LocalIP = "172.16.159.101"
Func _Put()
	;$text2base64 = "WE1MUGhvbmVDTUQ6bnYxMG0xZHBA"

	$hw_open = _WinHttpOpen()
	ConsoleWrite('WinHttpOpen: '&$hw_open&@CRLF)

	$hw_connect = _WinHttpConnect($hw_open, $ip)
	ConsoleWrite('WinHttpConnect: '&$hw_connect&@CRLF)

	$h_openRequest = _WinHttpOpenRequest($hw_connect, $G_P_Type, $CommandType)
	ConsoleWrite('_WinHttpOpenRequest: '&$h_openRequest&@CRLF)

	$wCreds = _WinHttpSetCredentials($h_openRequest, $WINHTTP_AUTH_TARGET_SERVER , $WINHTTP_AUTH_SCHEME_BASIC, $UserID, $Password)
	ConsoleWrite('_WinHttpSetCredentials: '&$wCreds&@CRLF)

	$sRequest = _WinHttpSendRequest($h_openRequest, '',$Data2, StringLen($Data2))
	ConsoleWrite('_WinHttpSendRequest: '&$sRequest&@CRLF)

	$rRequest = _WinHttpReceiveResponse($h_openRequest)
	ConsoleWrite('_WinHttpReceiveResponse: '&$rRequest&@CRLF)

	;If _WinHttpQueryDataAvailable($h_openRequest) Then
		$header = _WinHttpQueryHeaders($h_openRequest)
		ConsoleWrite('_WinHttpQueryHeaders: '&$header&@CRLF)
		;MsgBox(0, "Header", $header)
		$body = _WinHttpReadData($h_openRequest)
		ConsoleWrite('_WinHttpReadData: '&$body&@CRLF)
		;MsgBox(0,'Body', $body)
	;EndIf

	_WinHttpCloseHandle($h_openRequest)

	_WinHttpCloseHandle($hw_connect)

	_WinHttpCloseHandle($hw_open)
EndFunc


Func MyErrFunc()
	$HexNumber=hex($oMyError.number,8)
	Msgbox(0,"COM Test","We intercepted a COM Error !" & @CRLF & @CRLF & _
				 "err.description is: " & @TAB & $oMyError.description & @CRLF & _
				 "err.windescription:" & @TAB & $oMyError.windescription & @CRLF & _
				 "err.number is: " & @TAB & $HexNumber & @CRLF & _
				 "err.lastdllerror is: " & @TAB & $oMyError.lastdllerror & @CRLF & _
				 "err.scriptline is: " & @TAB & $oMyError.scriptline & @CRLF & _
				 "err.source is: " & @TAB & $oMyError.source & @CRLF & _
				 "err.helpfile is: " & @TAB & $oMyError.helpfile & @CRLF & _
				 "err.helpcontext is: " & @TAB & $oMyError.helpcontext _
				)
	SetError(1) ; to check for after this function returns
Endfunc




