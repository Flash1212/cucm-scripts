#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=Images\Cisco_VPN_Icon_by_rich90usa.ico
#AutoIt3Wrapper_Outfile=CUCM Migrator v7.exe
#AutoIt3Wrapper_Compile_Both=y
#AutoIt3Wrapper_UseX64=y
#AutoIt3Wrapper_Res_Fileversion=3.0.0.0
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

;~ Left off here: If BitAND(GUICtrlRead($CheckBox_Partition10), $GUI_CHECKED) Then Modify_Partitoins('10', $aMethod[3][1], $aMethod[14][1], $CUCM7, GUICtrlRead($Combo_Partitions_10_From_7, 1), 'uuid')

;CUCM Get IP

;GUI Functions
#include <GuiEdit.au3>
#include <GUIListBox.au3>
#include <GuiComboBox.au3>
#include <GuiListView.au3>
#include <GuiIPAddress.au3>
#include <GuiStatusBar.au3>
#include <XSkinAnimate.au3>
#include <ComboConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <ListViewConstants.au3>
#include <ProgressConstants.au3>

;Function Includes
#include <File.au3>
#include <Array.au3>
#include <String.au3>
#include <WinHTTP.au3>
#include <.\soap_xml.au3>
#include <.\SOAP_Open.au3>
;~ #include <.\PrettyXML.au3>

Opt('GUIOnEventMode', 1)
Opt('MustDeclareVars', 1)

;~ External
$SHOW1 = True
$SHOW2 = True

;~ Internal
Global $preRuns = True ;Determines whether or not PreRuns() executes

#Region Declarations ###################################################################
;~ GUI
Global	$CUCM_Migrator, $iGUIWidth = 1301, $iGUIHeight = 733, $ListGUI, $ExceptionList, $ListListview, $ExceptionListview

;~ Listview
Global 	$CUCM7, $CUCM10, _
		$exStyles = BitOR($LVS_EX_FULLROWSELECT, $LVS_EX_CHECKBOXES)

;~ Labels
Global 	$Label_CUCM7, $Label_CUCM10, $Label_IP_7, $Label_IP_10, $Label_DP_7, $Label_DP_10, $Label_Partitions_7, _
		$Label_Partitions_10, $Label_Reset_7, $Label_Reset_10

;~ Menu/StatusBars
Global 	$statusbar, $File_menu, $Export, $Clear_Field, $Separator1, $Exititem, _
		$View_menu, $View_Status, $View_DevicePools, $View_Partition, $View_Pattern, $View_Reset

;~ Buttons
Global 	$Button_Scan7, $Button_Scan10, $Button_All, $Button_Reset7, $Button_Reset_10, $Button_DP_7, $Button_DP_10, _
		$Button_Part_7, $Button_Part_10

;~ Edit/IP
Global	$IPAddress_CUCM7, $IPAddress_CUCM10, $Edit_Pattern_Desc_7, $Edit_Pattern_Desc_10

;~ Groups
Global	$Group7, $Group10

;~ Combos
Global	$Combo_Device_Pools_7, $Combo_Device_Pools_10, $Combo_Partitions_7, $Combo_Partitions_10, _
		$Combo_Patterns_7, $Combo_Patterns_10, $Combo_Registration_7, $Combo_Registration_10, _
		$Combo_SelectBy_7, $Combo_SelectBy_10

;~ Checkboxes
Global $CheckBox_Pattern_7, $CheckBox_Pattern_10, $CheckBox_All_7, $CheckBox_All_10, $CheckBox_Partition10, $Combo_Partitions_10_From_7, _
		$CheckBox_Import_7, $CheckBox_Import_10

;~ Misc
Global 	$nMsg, $LogFolder = @ScriptDir&'\Logs', $action, $resize = False, $7_Online = False, $10_Online = False, _
		$Images = @ScriptDir&'\Images', $iCon = $Images&'\Cisco_VPN_Icon_by_rich90usa.ico', $iLogo = $Images&'\SetonLogo.jpg'

;~ Posititioning
Global	$aText[3] = ["Status:", "7: ", "10: "], _
		$partStatus = 50, _ ;Statusbar
		$partCUCM7 = 652/1301, _ ;CUCM7
		$partCUCM10 = 650/1301, _ ;CUCM10
		$aParts[3] = [$partStatus, $partCUCM7 * $iGUIWidth, $partCUCM10 * $iGUIWidth]

Global	$Top_POS[9] = [ 393, _ 	;IP[0]
						448, _ 	;DevPool[1]
						503, _	;Partitions[2]
						538, _	;Patterns[3]
						596, _	;Reset[4]
						346, _	;Group Box[5]
						50, _	;Listview[6] ; Original = 34
						10	]	;CUCM Labels[7] ; Original = 8

Global	$CUCM7_Group_Left = 6, _ 	;Left Pos for CUCM 7[0]
		$CUCM10_Group_Left = 655, _	;Left Pos for CUCM 10[1]
		$CUCM_Group_W = 639, _		;Width for CUCM Groups[2]
		$CUCM_Group_H = 305			;Height for CUCM Groups[3]

Global	$GUIobj[6] = [	6, _								;Labels[0]
						269, _								;IP Fields[1]
						375, _								;SelectBy[2]
						401, _								;DropBoxes[3]
						552, _								;Buttons[4]
						150	]								;CheckBox[5]

Global	$L_POS_7[6] =  [$CUCM7_Group_Left + $GUIobj[0], _ 	;All Lables[0]
						$CUCM7_Group_Left + $GUIobj[1], _	;IPs Fields[1]
						$CUCM7_Group_Left + $GUIobj[2], _	;SelectBy[2]
						$CUCM7_Group_Left + $GUIobj[3], _	;DropBoxes[3]
						$CUCM7_Group_Left + $GUIobj[4], _	;All Buttons[4]
						$CUCM7_Group_Left + $GUIobj[5]]		;CheckBox[5]

Global	$L_POS_10[6] = [$CUCM10_Group_Left + $GUIobj[0], _ 	;All Lables[0]
						$CUCM10_Group_Left + $GUIobj[1], _	;IPs Fields[1]
						$CUCM10_Group_Left + $GUIobj[2], _	;SelectBy[2]
						$CUCM10_Group_Left + $GUIobj[3], _	;DropBoxes[3]
						$CUCM10_Group_Left + $GUIobj[4], _	;All Buttons[4]
						$CUCM10_Group_Left + $GUIobj[5]]	;CHeckBox[5]

Global	$idListWidth = 639, $idListHeight = 292, $idListColCountWidth = 47, $idListRatio = (47/639)

;~ Colors
Global	$cText = 0xF1ECDF, $cList = 0xD6D7D9, _
		$MainGUIBKColor = 0x575757; 0x1C1C1F; 0x0121314 ; 0x5C5F58

;Export GUI
Global	$Export_GUI, $ExportType, $Radio_7, $Radio_10, $Radio_Both, $Group_Export, $Button_Export, $Button_Cancel

#EndRegion Declarations ###############################################################

;Check that CUCM 7 & CUCM 10 are up and running
Func _ServerOnline()
	Local	$ping

	$ping = Ping("UCM01")
	If @error Then
		MsgBox(16,'Fatal Error', "Unable to detect Unified Communications Manager 7.")
		_Status("Unable to detect Unified Communications Manager 7.", '7')
		$7_Online = False
	Else
		_Status("Detected Unified Communications Manager 7.", '7')
		$7_Online = True
	EndIf

	$ping = Ping("txausdcmcucm01")
	If @error Then
		MsgBox(16,'Fatal Error', "Unable to detect Unified Communications Manager 10.")
		_Status("Unable to detect Unified Communications Manager 10.", '10')
		$10_Online = False
	Else
		_Status("Detected Unified Communications Manager 10.", '10')
		$10_Online = True
	EndIf

EndFunc

;Main Magic
DirCreate($Images)
FileInstall("G:\is-voip\djthornton\Scripts\CUCM Scripts\Post To CUCM\Images\Cisco_VPN_Icon_by_rich90usa.ico", $iCon, 1)
FileInstall("G:\is-voip\djthornton\Scripts\CUCM Scripts\Post To CUCM\Images\SetonLogo.jpg", $iLogo, 1)

_GUI()

#Region GUI ############################################################################

Func _GUI()
	#Region ### START Koda GUI section ### Form=G:\is-voip\djthornton\Scripts\CUCM Scripts\Post To CUCM\CUCM Migration Tool.kxf
;~ 	GUI
	#Region Non-Specific
	$CUCM_Migrator = GUICreate("CUCM Migrator v.7", $iGUIWidth, $iGUIHeight, -1, -1, $WS_OVERLAPPEDWINDOW)
	GUISetIcon($iCon)
	GUISetOnEvent($GUI_EVENT_CLOSE, '_Exit', $CUCM_Migrator)
;~ 	GUICtrlSetResizing($CUCM_Migrator, $GUI_DOCKAUTO)
	GUISetBkColor($MainGUIBKColor, $CUCM_Migrator)

;~ 	FileMenu
	$File_menu = GUICtrlCreateMenu("&File")
	GUICtrlSetBkColor(-1, 0x585858)
	GUICtrlCreateMenuItem("Import IP List", $File_menu)
	GUICtrlSetOnEvent(-1, '_Import_IP_GUI')
	GUICtrlCreateMenuItem("Exception IP List", $File_menu)
	GUICtrlSetOnEvent(-1, '_Exception_IP_GUI')
	$Export = GUICtrlCreateMenuItem("Export", $File_menu)
	GUICtrlSetOnEvent(-1, '_Export_GUI')
	$Clear_Field = GUICtrlCreateMenuItem("Clear Fields", $File_menu)
	GUICtrlSetOnEvent(-1, '_Clear')
	$Separator1 = GUICtrlCreateMenuItem("", $File_menu, 3)
	$Exititem = GUICtrlCreateMenuItem("Exit", $File_menu)
	GUICtrlSetOnEvent(-1, '_Exit')
	GUICtrlSetState($File_menu, @SW_SHOW)

;~ 	ViewMenu
	$View_menu = GUICtrlCreateMenu("&View")
	$View_Status = GUICtrlCreateMenuItem("View Log Folder", $View_menu)
	GUICtrlSetOnEvent(-1, '_CallLogFolder')
	GUICtrlSetState($View_menu, @SW_SHOW)

;~ 	Labels
	$Label_CUCM7 = GUICtrlCreateLabel("Unified Communications Manager 7", $CUCM7_Group_Left, $Top_POS[8], 500, 25)
	GUICtrlSetFont(-1, 16, 800, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)

	$Label_CUCM10 = GUICtrlCreateLabel("Unified Communications Manager 10", $CUCM10_Group_Left, $Top_POS[8], 500, 25)
	GUICtrlSetFont(-1, 16, 800, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)

;~ 	Buttons
	$Button_All = GUICtrlCreateButton("Scan Both", 612, 658, 75, 25)
	GUICtrlSetOnEvent(-1, '_CallBoth')
	#EndRegion Non Specific

	#Region CUCM 7
;~ 	***************Begin Group7********************
;~ 	$Group7 = GUICtrlCreateGroup("CUCM 7", 6, 346, 639, 305)
	$Group7 = _XSkinGroup("CUCM 7", $CUCM7_Group_Left, $Top_POS[5], $CUCM_Group_W, $CUCM_Group_H, $cText)
	GUICtrlSetFont(-1, 14, 800, 0, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)

;~ 	Edit/IPs
	$IPAddress_CUCM7 = GUICtrlCreateInput("", $L_POS_7[1], $Top_POS[0], 90, 21)
	$Edit_Pattern_Desc_7 = GUICtrlCreateInput("", $L_POS_7[1], $Top_POS[3], 90, 21)


;~ 	Labels
	$Label_IP_7 = GUICtrlCreateLabel("IP Address", $L_POS_7[0], $Top_POS[0], 92, 24)
	GUICtrlSetFont(-1, 12, 800, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)
	$Label_DP_7 = GUICtrlCreateLabel("Device Pools", $L_POS_7[0], $Top_POS[1], 107, 24)
	GUICtrlSetFont(-1, 12, 800, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)
	$Label_Partitions_7 = GUICtrlCreateLabel("Partitions", $L_POS_7[0], $Top_POS[2], 80, 24)
	GUICtrlSetFont(-1, 12, 800, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)
	$Label_Reset_7 = GUICtrlCreateLabel("Reset Phones", $L_POS_7[0], $Top_POS[4], 117, 24)
	GUICtrlSetFont(-1, 12, 800, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)

;~ 	Combos
	$Combo_SelectBy_7 = GUICtrlCreateCombo("IpAddress", $L_POS_7[2], $Top_POS[0], 75, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
	$Combo_Registration_7 = GUICtrlCreateCombo("Registered", $L_POS_7[3] + 60, $Top_POS[0], 85, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
	$Combo_Device_Pools_7 = GUICtrlCreateCombo("Device Pools", $L_POS_7[3], $Top_POS[1], 145, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
	$Combo_Partitions_7 = GUICtrlCreateCombo("Partitions", $L_POS_7[3], $Top_POS[2], 145, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
	$Combo_Patterns_7 = GUICtrlCreateCombo("Pattern Partition", $L_POS_7[3], $Top_POS[3], 145, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
;~ 	GUICtrlSetState($Combo_Patterns_7, @SW_DISABLE)
	$Combo_Partitions_10_From_7 = GUICtrlCreateCombo("10.5 Partition", $L_POS_7[3], $Top_POS[3] + 28, 145, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
;~ 	GUICtrlSetState($Combo_Partitions_10_From_7, @SW_DISABLE)

;~ 	Checkboxes
	$CheckBox_All_7 = GUICtrlCreateCheckbox("All", $L_POS_7[0], $Top_POS[8] + 25, 110, 25)
	GUICtrlSetFont(-1, 10, 600, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)
	DllCall("UxTheme.dll", "int", "SetWindowTheme", "hwnd", GUICtrlGetHandle($CheckBox_All_7), "wstr", 0, "wstr", 0)
	GUICtrlSetState($CheckBox_All_7, $GUI_CHECKED)
	GUICtrlSetOnEvent($CheckBox_All_7, '_CallCheckAll7')
	$CheckBox_Pattern_7 = GUICtrlCreateCheckbox("Add CUCM7 Pattern", $L_POS_7[5], $Top_POS[3], 114, 25)
	GUICtrlSetOnEvent(-1, '_CallCheckBox7')
	GUICtrlSetColor(-1, $cText)
	DllCall("UxTheme.dll", "int", "SetWindowTheme", "hwnd", GUICtrlGetHandle($CheckBox_Pattern_7), "wstr", 0, "wstr", 0)
	$CheckBox_Partition10 = GUICtrlCreateCheckbox("Update CUCM10", $L_POS_7[1], $Top_POS[3] + 28, 100, 21)
	GUICtrlSetOnEvent(-1, '_CheckBox_Partition10')
	GUICtrlSetColor(-1, $cText)
	DllCall("UxTheme.dll", "int", "SetWindowTheme", "hwnd", GUICtrlGetHandle($CheckBox_Partition10), "wstr", 0, "wstr", 0)
	$CheckBox_Import_7 = GUICtrlCreateCheckbox("Use Import List", $L_POS_7[1], $Top_POS[0]+25, 110, 25)
	GUICtrlSetOnEvent(-1, '_Import_CheckBox')
	GUICtrlSetColor(-1, $cText)
	DllCall("UxTheme.dll", "int", "SetWindowTheme", "hwnd", GUICtrlGetHandle($CheckBox_Import_7), "wstr", 0, "wstr", 0)

;~ 	Buttons
	$Button_Scan7 = GUICtrlCreateButton("Scan", $L_POS_7[4], $Top_POS[0] - 2, 75, 25)
	GUICtrlSetOnEvent(-1, '_CallScan7')
	$Button_DP_7 = GUICtrlCreateButton("Apply", $L_POS_7[4], $Top_POS[1] - 2, 75, 25)
	GUICtrlSetOnEvent(-1, '_CallPool7')
	$Button_Part_7 = GUICtrlCreateButton("Apply", $L_POS_7[4], $Top_POS[2] - 2, 75, 25)
	GUICtrlSetOnEvent(-1, '_CallPart7')
	$Button_Reset7 = GUICtrlCreateButton("Reset", $L_POS_7[4], $Top_POS[4] - 2, 75, 25)
	GUICtrlSetOnEvent(-1, '_CallReset7')

	GUICtrlCreateGroup("", -99, -99, 1, 1)
	#EndRegion CUCM 7
;~ 	***************End of Group7********************

	#Region CUCM 10
;~ 	***************Begin Group10*********************
;~ 	$Group10 = GUICtrlCreateGroup("CUCM 10", 655, 346, 639, 305)
	$Group10 = _XSkinGroup("CUCM 10", $CUCM10_Group_Left, $Top_POS[5], $CUCM_Group_W, $CUCM_Group_H, $cText)
	GUICtrlSetFont(-1, 14, 800, 0, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)

;~ 	Edit/IPs
	$IPAddress_CUCM10 = GUICtrlCreateInput("", $L_POS_10[1], $Top_POS[0], 90, 21)
	$Edit_Pattern_Desc_10 = GUICtrlCreateInput("", $L_POS_10[1], $Top_POS[3], 90, 21)

;~ 	Labels
	$Label_IP_10 = GUICtrlCreateLabel("IP Address", $L_POS_10[0], $Top_POS[0], 92, 24)
	GUICtrlSetFont(-1, 12, 800, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)
	$Label_DP_10 = GUICtrlCreateLabel("Device Pools", $L_POS_10[0], $Top_POS[1], 107, 24)
	GUICtrlSetFont(-1, 12, 800, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)
	$Label_Partitions_10 = GUICtrlCreateLabel("Partitions", $L_POS_10[0], $Top_POS[2], 80, 24)
	GUICtrlSetFont(-1, 12, 800, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)
	$Label_Reset_10 = GUICtrlCreateLabel("Reset Phones", $L_POS_10[0], $Top_POS[4], 117, 24)
	GUICtrlSetFont(-1, 12, 800, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)

;~
	$Combo_SelectBy_10 = GUICtrlCreateCombo("IpAddress", $L_POS_10[2], $Top_POS[0],75, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
	$Combo_Registration_10 = GUICtrlCreateCombo("Registered", $L_POS_10[3] + 60, $Top_POS[0], 85, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
	$Combo_Device_Pools_10 = GUICtrlCreateCombo("Device Pools", $L_POS_10[3], $Top_POS[1], 145, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
	$Combo_Partitions_10 = GUICtrlCreateCombo("Paritions", $L_POS_10[3], $Top_POS[2], 145, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
	$Combo_Patterns_10 = GUICtrlCreateCombo("Pattern Paritions", $L_POS_10[3], $Top_POS[3], 145, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
	GUICtrlSetState(-1, @SW_DISABLE)

;~ 	Checkboxes
	$CheckBox_All_10 = GUICtrlCreateCheckbox("All", $L_POS_10[0], $Top_POS[8] + 25, 110, 25)
	GUICtrlSetFont(-1, 10, 600, 4, "MS Sans Serif")
	GUICtrlSetColor(-1, $cText)
	DllCall("UxTheme.dll", "int", "SetWindowTheme", "hwnd", GUICtrlGetHandle($CheckBox_All_10), "wstr", 0, "wstr", 0)
	GUICtrlSetState($CheckBox_All_10, $GUI_CHECKED)
	GUICtrlSetOnEvent($CheckBox_All_10, '_CallCheckAll10')
	$CheckBox_Pattern_10 = GUICtrlCreateCheckbox("Add Route Pattern", $L_POS_10[5], $Top_POS[3], 110, 25)
	GUICtrlSetOnEvent(-1, '_CallCheckBox10')
	GUICtrlSetColor(-1, $cText)
	GUICtrlSetState(-1, $GUI_DISABLE)
	DllCall("UxTheme.dll", "int", "SetWindowTheme", "hwnd", GUICtrlGetHandle($CheckBox_Pattern_10), "wstr", 0, "wstr", 0)
	$CheckBox_Import_10 = GUICtrlCreateCheckbox("Use Import List", $L_POS_10[1], $Top_POS[0]+25, 110, 25)
	GUICtrlSetOnEvent(-1, '_Import_CheckBox')
	GUICtrlSetColor(-1, $cText)
	DllCall("UxTheme.dll", "int", "SetWindowTheme", "hwnd", GUICtrlGetHandle($CheckBox_Import_10), "wstr", 0, "wstr", 0)

;~ 	Buttons
	$Button_Scan10 = GUICtrlCreateButton("Scan", $L_POS_10[4], $Top_POS[0] - 2, 75, 25)
	GUICtrlSetOnEvent(-1, '_CallScan10')
	$Button_DP_10 = GUICtrlCreateButton("Apply", $L_POS_10[4], $Top_POS[1] - 2, 75, 25)
	GUICtrlSetOnEvent(-1, '_CallPool10')
	$Button_Part_10 = GUICtrlCreateButton("Apply", $L_POS_10[4], $Top_POS[2] - 2, 75, 25)
	GUICtrlSetOnEvent(-1, '_CallPart10')
	$Button_Reset_10 = GUICtrlCreateButton("Reset", $L_POS_10[4], $Top_POS[4] - 2, 75, 25)
	GUICtrlSetOnEvent(-1, '_CallReset10')
	#EndRegion CUCM 10
;~ 	******************End Group10******************
	GUICtrlCreateGroup("", -99, -99, 1, 1)
	#Region ListView
;~ 	ListViews
	$CUCM7 = GUICtrlCreateListView("|Name|IP|Status|Description", $L_POS_7[0] - 6, $Top_POS[6], $idListWidth, $idListHeight, -1)
	GUICtrlSetOnEvent($CUCM7, '_CallSort7')
	_GUICtrlListView_SetExtendedListViewStyle($CUCM7, BitOR($LVS_EX_FULLROWSELECT, $LVS_EX_GRIDLINES, $LVS_EX_DOUBLEBUFFER, $LVS_EX_CHECKBOXES))
	_GUICtrlListView_SetBkColor($CUCM7, $cList)
	_GUICtrlListView_SetTextBkColor($CUCM7, $cList)


	$CUCM10 = GUICtrlCreateListView("|Name|IP|Status|Description", $L_POS_10[0] - 6, $Top_POS[6], $idListWidth, $idListHeight, -1)
	GUICtrlSetOnEvent($CUCM10, '_CallSort10')
	_GUICtrlListView_SetExtendedListViewStyle($CUCM10, BitOR($LVS_EX_FULLROWSELECT, $LVS_EX_GRIDLINES, $LVS_EX_SUBITEMIMAGES, $LVS_EX_CHECKBOXES))
	_GUICtrlListView_SetBkColor($CUCM10, $cList)
	_GUICtrlListView_SetTextBkColor($CUCM10, $cList)
	#EndRegion Listview
;~ 	StatusBar
	$statusbar = _GUICtrlStatusBar_Create($CUCM_Migrator, $aParts, $aText)
	GUIRegisterMsg($WM_SIZE, "WM_SIZE")

	_GUICtrlListView_RegisterSortCallBack($CUCM7)
	_GUICtrlListView_RegisterSortCallBack($CUCM10)

	_ServerOnline()
	_ImageIn_and_Out()
	WinSetTrans($CUCM_Migrator, '', 0)
	GUISetState(@SW_SHOW, $CUCM_Migrator)
#EndRegion ############################################################################

	;Secondary GUIs
	;Import Lis
	#Region Secondary GUIs
	$ListGUI = GUICreate("IP List", 200, 316, 749, 170, -1, -1, $CUCM_Migrator)
	GUISetIcon($iCon)
	GUISetOnEvent($GUI_EVENT_CLOSE, '_Exit_IP_List', $ListGUI)
	$ListListview = GUICtrlCreateEdit("IP/Name", 0, 0, 305, 305)
	GUICtrlSetOnEvent(-1, '_Rem_WhiteSpace')

	;Exception List
	$ExceptionList = GUICreate("IP Exceptions", 200, 316, 749, 170, -1, -1, $CUCM_Migrator)
	GUISetIcon($iCon)
	GUISetOnEvent($GUI_EVENT_CLOSE, '_Exit_IP_Exceptions', $ExceptionList)
	$ExceptionListview = GUICtrlCreateEdit("IP/Name", 0, 0, 305, 305)
	GUICtrlSetOnEvent(-1, '_Rem_WhiteSpace')

#EndRegion ### END Secondary GUI section ###

	For $x = 0 To 255 Step 5
		WinSetTrans($CUCM_Migrator, '', $x)
		Sleep(1)
	Next

	DirCreate($LogFolder)

	While 1
		Sleep(250)
		If $resize = True Then
			_ColWidth()
			$resize = False
		EndIf
;~ 		_ServerOnline()
	WEnd

EndFunc

; Resize the status bar when GUI size changes
Func WM_SIZE($hWnd, $iMsg, $wParam, $lParam)
	#forceref $hWnd, $iMsg, $wParam, $lParam
	Local	$aGetGUIWidth
	_GUICtrlStatusBar_Resize($statusbar)
	$aGetGUIWidth = WinGetPos('CUCM Migrator')
	$iGUIWidth = $aGetGUIWidth[2]
	$aParts[1] = ($partCUCM7 * $iGUIWidth)
	$aParts[2] = ($partCUCM10* $iGUIWidth)
	_GUICtrlStatusBar_SetParts($statusbar, $aParts)
;~ 	If $boot = False Then _ColWidth()
	$resize = True
	Return $GUI_RUNDEFMSG
EndFunc   ;==>WM_SIZE

;~ Export data
Func _Export_GUI()

	#Region ### START Koda GUI section ### Form=g:\is-voip\djthornton\scripts\cucm scripts\post to cucm\export.kxf
	$Export_GUI = GUICreate("eXport", 227, 168, 1111, 430, Default, Default, $CUCM_Migrator)
	GUISetIcon($iCon)
	GUISetOnEvent($GUI_EVENT_CLOSE, '_Close_Export', $Export_GUI)
	$ExportType = GUICtrlCreateLabel("Which table do you want to export?", 28, 24, 169, 17)
	$Radio_7 = GUICtrlCreateRadio("CUCM 7", 24, 80, 65, 17)
	$Radio_10 = GUICtrlCreateRadio("CUCM 10", 90, 80, 70, 17)
	$Radio_Both = GUICtrlCreateRadio("Both", 168, 80, 41, 17)
	$Group_Export = GUICtrlCreateGroup("Export", 8, 56, 209, 57)
	GUICtrlCreateGroup("", -99, -99, 1, 1)
	$Button_Export = GUICtrlCreateButton("Export", 19, 128, 75, 25)
	GUICtrlSetOnEvent(-1, "_Export")
	$Button_Cancel = GUICtrlCreateButton("Cancel", 136, 128, 75, 25)
	GUICtrlSetOnEvent(-1, '_Close_Export')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

EndFunc

Func _Import_CheckBox()

	If BitAND(GUICtrlRead($CheckBox_Import_7), $GUI_CHECKED) Or BitAND(GUICtrlRead($CheckBox_Import_10), $GUI_CHECKED) Then _Import_IP_GUI()

EndFunc

Func _Import_IP_GUI()

	GUISetState(@SW_SHOW, $ListGUI)

EndFunc

Func _Exception_IP_GUI()

	GUISetState(@SW_SHOW, $ExceptionList)

EndFunc

#EndRegion GUI #########################################################################

#Region Intro ##########################################################################

;===============================================================================
; Name:   _ImageIn_and_Out()
; Description:   Welcome GUI Seton Logo
; Syntax:   _ImageIn_and_Out()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _ImageIn_and_Out()
	Local $Intro, $Pic, $Progress

	$Intro = GUICreate('Change', 300, 320, Default, Default, $WS_POPUP, BitOR($WS_EX_TOOLWINDOW, $WS_EX_TOPMOST), $CUCM_Migrator)
	$Pic = GUICtrlCreatePic('', -1, -1, 300, 300)
	$Progress = GUICtrlCreateProgress(0, 301, 300, 20, $PBS_MARQUEE)
	GUICtrlSendMsg($progress, $PBM_SETMARQUEE, 1, 0)
	GUISetState(@SW_HIDE)
	GUICtrlSetImage($Pic, $iLogo)
	For $x = 0 To 200 Step 5
		WinSetTrans($Intro, '', $x)
		If $x = 0 Then GUISetState(@SW_SHOW, $Intro)
		Sleep(1)
	Next
;~ 	Sleep(1000)

	If $preRuns = True Then _Preruns()

;~ 	Sleep(1000)
	For $x = 200 To 0 Step -10
		WinSetTrans($Intro, '', $x)
		Sleep(1)
	Next
	GUIDelete($Intro)
EndFunc   ;==>_ImageIn_and_Out

;===============================================================================
; Name:   _Preruns()
; Description:    Run CUCM Modules for Device Pool and Partions.
; Syntax:   _Preruns()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _Preruns()

	If $7_Online = True Then
		_Status('Loading Device Pool Module', '7')
		_ListDevicePoolByName()
		_Status('Device Pool Module Loaded', '7')
	EndIf
	Sleep(1000)
	If $10_Online = True Then
		_Status('Loading Device Pool Module', '10')
		_ListDevicePool()
		_Status('Device Pool Module Loaded', '10')
	EndIf
	Sleep(1000)
	If $7_Online = True Then
		_Status('Loading Partition Module', '7')
		_ListRoutePartitionByName()
		_CallCheckBox7()
		_CheckBox_Partition10()
		_Status('Partition Module Loaded', '7')
	EndIf
	Sleep(1000)
	If $10_Online = True Then
		_Status('Loading Partition Module', '10')
		_ListRoutePartition()
		_CallCheckBox10()
		_Status('Partition Module Loaded', '10')
	EndIf

	;Always run
	_ColWidth()
	_Registration_Selection()

EndFunc

;===============================================================================
; Name:   _ColWidth()
; Description:   Set Column Widths
; Syntax:   _ColWidth()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _ColWidth()
	Local	$Pos

	$Pos = ControlGetPos('', '', $CUCM7)
;~ 	If @error Then MsgBox(16,'Resize Error', 'Error: '&@error&@CRLF&'Extended error: '&@extended)



	If Not IsArray($Pos) Or UBound($Pos) <> 4 Then
		ConsoleWrite('Resize Error'&@CRLF)
		Return
	Else
		$idListWidth = $Pos[2]
	EndIf

	$idListColCountWidth = $idListWidth*$idListRatio

	;CUCM7
	_GUICtrlListView_SetColumnWidth($CUCM7, 0, $idListColCountWidth)
	For $iCol = 1 To 3
		_GUICtrlListView_SetColumnWidth($CUCM7, $iCol, (($idListWidth - $idListColCountWidth)/6))
	Next
	_GUICtrlListView_SetColumnWidth($CUCM7, 4, $idListWidth - (($idListWidth - $idListColCountWidth)/6)*3.5)

	;CUCM10
	_GUICtrlListView_SetColumnWidth($CUCM10, 0, $idListColCountWidth)
	For $iCol = 1 To 3
		_GUICtrlListView_SetColumnWidth($CUCM10, $iCol, (($idListWidth - $idListColCountWidth)/6))
	Next
	_GUICtrlListView_SetColumnWidth($CUCM10, 4, $idListWidth - (($idListWidth - $idListColCountWidth)/6)*3.5)
EndFunc

;===============================================================================
; Name:   _ListDevicePoolByName()
; Description:   Get Device Pool Information from CUCM 7
; Syntax:   _ListDevicePoolByName()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _ListDevicePoolByName()
	Local	$version = '7', $aXML, $return, $returnParsed, $LogFile = $LogFolder&'\Device Pool 7.Log'

	_Status('Building Device Pool List', $version)

	$return = _Call($version, $aMethod[0][0], '%')

	$returnParsed = _Find($return, 'poolparts')

	If $returnParsed = 'Error' Then
		_Log($LogFile, 'Error building CUCM '&$version&' Device Pool list,'&$return)
		_Status( 'Error building CUCM '&$version&' Device Pool list,'&$return, $version)
		MsgBox(16,'Error', 'There was an error in the return. The XML return has been logged.')
		Return
	EndIf

	For $x = 0 To UBound($returnParsed) - 1
		_GUICtrlComboBox_AddString($Combo_Device_Pools_7, $returnParsed[$x])
	Next

	_Status('Build of Device Pool List Complete', $version)

EndFunc

;===============================================================================
; Name:   _ListDevicePool()
; Description:   Get Device Pool Information from CUCM 10
; Syntax:   _ListDevicePool()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _ListDevicePool()
	Local	$version = '10', $aXML, $return, $returnParsed, $LogFile = $LogFolder&'\Device Pool 10.Log'

	_Status('Building Device Pool List', $version)

	$return = _Call($version, $aMethod[0][1], '%')
;~ 	MsgBox(64,'Device Pool 10', "return: "&$return)

	$returnParsed = _Find($return, 'poolparts')
;~ 	_ArrayDisplay($returnParsed, 'parsed')
;~ 	MsgBox(64,'Device Pool 10', "parsed: "&$returnParsed)

	If $returnParsed = 'Error' Then
		_Log($LogFile, 'Error building CUCM '&$version&' Device Pool list,'&$return)
		_Status('Error building CUCM '&$version&' Device Pool list,'&$return, $version)
		MsgBox(16,'Error', 'There was an error in the return. The XML return has been logged.')
		Return
	EndIf

	For $x = 0 To UBound($returnParsed) - 1
		_GUICtrlComboBox_AddString($Combo_Device_Pools_10, $returnParsed[$x])
	Next

	_Status('Build of Device Pool List Complete', $version)

EndFunc

;===============================================================================
; Name:   _ListRoutePartitionByName()
; Description:   Get Device Pool Information from CUCM 7
; Syntax:   _ListRoutePartitionByName()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _ListRoutePartitionByName()
	Local	$version = '7', $aXML, $return, $returnParsed, $LogFile = $LogFolder&'\Route Partition 7.Log'

	_Status('Building Partition List', $version)

	$return = _Call($version, $aMethod[12][0], '%')

	$returnParsed = _Find($return, 'poolparts')

	If $returnParsed = 'Error' Then
		_Log($LogFile, 'Error building CUCM '&$version&' partition list,'&$return)
		_Status('Error building CUCM '&$version&' partition list,'&$return, $version)
		MsgBox(16,'Error', 'There was an error in the return. The XML return has been logged.')
		Return
	EndIf

	For $x = 0 To UBound($returnParsed) - 1
		_GUICtrlComboBox_AddString($Combo_Partitions_7, $returnParsed[$x])
		_GUICtrlComboBox_AddString($Combo_Patterns_7, $returnParsed[$x])
	Next

	_Status('Build of Partitions List Complete', $version)

EndFunc

;===============================================================================
; Name:   _ListRoutePartition()
; Description:   Get Device Pool Information from CUCM 10
; Syntax:   _ListRoutePartition()
; Parameter(s):   None
; Requirement(s):   None
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):
; Example(s):
;===============================================================================
Func _ListRoutePartition()
	Local	$version = '10', $aXML, $return, $returnParsed, $LogFile = $LogFolder&'\Route Partition 10.Log'

	_Status('Building Partition List', $version)

	$return = _Call($version, $aMethod[12][1], '%')

	$returnParsed = _Find($return, 'poolparts')

	If $returnParsed = 'Error' Then
		_Status('Error building CUCM '&$version&' partition list,'&$return, $version)
		MsgBox(16,'Error', 'There was an error in the return. The XML return has been logged.')
		Return
	EndIf

	For $x = 0 To UBound($returnParsed) - 1
		_GUICtrlComboBox_AddString($Combo_Partitions_10, $returnParsed[$x])
		_GUICtrlComboBox_AddString($Combo_Patterns_10, $returnParsed[$x])
		_GUICtrlComboBox_AddString($Combo_Partitions_10_From_7, $returnParsed[$x])
	Next

	_Status('Build of Partitions List Complete', $version)

EndFunc

Func _Registration_Selection()
	;CUCM 7
	_GUICtrlComboBox_AddString($Combo_SelectBy_7, "Name" )
	_GUICtrlComboBox_AddString($Combo_Registration_7, "Unregistered" )

	;CUCM 10
	_GUICtrlComboBox_AddString($Combo_SelectBy_10, "Name" )
	_GUICtrlComboBox_AddString($Combo_Registration_10, "Unregistered" )
EndFunc

#EndRegion Intro ##########################################################################

#Region IP Scan ########################################################################
Func _CallBoth()
	Local	$IP7, $IP10, $ImportCheckBox = 0, $return = 'good to go'

	$IP7 = GUICtrlRead($IPAddress_CUCM7, 1)
	$IP10 = GUICtrlRead($IPAddress_CUCM10, 1)

;~ 	_CallScan7()
;~ 	_CallScan10()


	If BitAND(GUICtrlRead($CheckBox_Import_7), $GUI_CHECKED) Or BitAND(GUICtrlRead($CheckBox_Import_10), $GUI_CHECKED) Then
;~ 		MsgBox(64,'1','here')
		_CallScan7()
		If $return = 'good to go' Then _CallScan10()

	ElseIf ($IP7 = '' And $IP10 = '') Then
;~ 		MsgBox(64,'2','here')
		MsgBox(16,'No IP','Please enter a valid IP address first.')
		Return

	ElseIf $IP7 <> '' And $IP10 <> '' Then
;~ 		MsgBox(64,'3','here')
		_CallScan7()
		_CallScan10()

	ElseIf	($IP7 <> '' And $IP10 = '') Then
;~ 		MsgBox(64,'4','here')
		_Scan('7', $aMethod[10][0], $CUCM7, $IP7, GUICtrlRead($Combo_Registration_7, 1), GUICtrlRead($Combo_SelectBy_7, 1), $CheckBox_All_7)
		If $return = 'good to go' Then _Scan('10', $aMethod[10][1], $CUCM10, $IP7, GUICtrlRead($Combo_Registration_7, 1), GUICtrlRead($Combo_SelectBy_7, 1), $CheckBox_All_10)

	ElseIf	$IP7 = '' And $IP10 <> '' Then
;~ 		MsgBox(64,'5','here')
		_Scan('7', $aMethod[10][0], $CUCM7, $IP10, GUICtrlRead($Combo_Registration_7, 1), GUICtrlRead($Combo_SelectBy_10, 1), $CheckBox_All_7)
		If $return = 'good to go' Then _Scan('10', $aMethod[10][1], $CUCM10, $IP10, GUICtrlRead($Combo_Registration_10, 1), GUICtrlRead($Combo_SelectBy_10, 1), $CheckBox_All_10)

	Else
;~ 		MsgBox(64,'6','here')
		MsgBox(16,'Unknown Error', 'Please contact your System Administrator.')
		Return

	EndIf

EndFunc

Func _CallScan7()
	Local	$IP

	$IP = GUICtrlRead($IPAddress_CUCM7, 1)

	If $IP = '' And (BitAND(GUICtrlRead($CheckBox_Import_7), $GUI_UNCHECKED) And BitAND(GUICtrlRead($CheckBox_Import_10), $GUI_UNCHECKED))  Then
		MsgBox(16,'No IP','Please enter a valid IP address first.')
		Return
	EndIf

	_Scan('7', $aMethod[10][0], $CUCM7, $IP, GUICtrlRead($Combo_Registration_7, 1), GUICtrlRead($Combo_SelectBy_7, 1), $CheckBox_All_7)

EndFunc

Func _CallScan10()
	Local $IP

	$IP = GUICtrlRead($IPAddress_CUCM10, 1)

	If $IP = '' And (BitAND(GUICtrlRead($CheckBox_Import_7), $GUI_UNCHECKED) And BitAND(GUICtrlRead($CheckBox_Import_10), $GUI_UNCHECKED))  Then
		MsgBox(16,'No IP','Please enter a valid IP address first.')
		Return
	EndIf

	_Scan('10', $aMethod[10][1], $CUCM10, $IP, GUICtrlRead($Combo_Registration_10, 1), GUICtrlRead($Combo_SelectBy_10, 1), $CheckBox_All_10)

EndFunc

Func _Scan($version, $method, $idListview, $IP, $regStatus, $SelectBy, $checkedboxAll)
	Local $return, $returnParsed, $aXML, $irow = 0

	_Status('Scanning CUCM IPs.', $version)

	GUICtrlSetState($checkedboxAll, $GUI_CHECKED)

	_GUICtrlListView_DeleteAllItems($idListview)


	If BitAND(GUICtrlRead($CheckBox_Import_7), $GUI_CHECKED) Or BitAND(GUICtrlRead($CheckBox_Import_10), $GUI_CHECKED) Then

		$IP = GUICtrlRead($ListListview, 1)
		$IP = StringReplace($IP, @CRLF, ',')
		If $IP = 'IP/Name' Then
			MsgBox(16,'No IP/Name','Please enter a valid IP address or name first.')
			Return 'no go list'
		EndIf
	EndIf

	$return = _Call_RIS($version, $method, $IP, $regStatus, $SelectBy)
;~ 	MsgBox(64,'Test', 'Scan 7: '&$return)

	$returnParsed = _Find($return, 'scanner')

	$returnParsed = _Exceptions($returnParsed)

	If Not IsArray($returnParsed) Then
		_GUICtrlListView_AddItem($idListview, '1')
		_GUICtrlListView_AddSubItem($idListview, 0, 'No Phones Found', 1)
		_Status('Scanning complete. 0 phones found.', $version)
		Return
	EndIf

	_GUICtrlListView_AddArray($idListview, $returnParsed)

	For $x = 0 To UBound($returnParsed) - 1
		_GUICtrlListView_SetItemChecked($idListview, $x)
	Next

	$irow = _GUICtrlListView_GetItemCount($idListview)

	_Status('Scanning complete. '& $irow & ' phones found.', $version)

	Return 'good to go'

EndFunc

Func _Exceptions($aArray)
	Local	$nameIP, $found

	$nameIP = GUICtrlRead($ExceptionListview, 1)
	$nameIP = StringSplit($nameIP, @CRLF, $STR_ENTIRESPLIT)
	_ArrayDelete($nameIP, 0)
;~ 	_ArrayDisplay($nameIP, 'nameIP')

	For $entry = 0 To UBound($nameIP) - 1

		$found = _ArraySearch($aArray, $nameIP[$entry], 0, 0, 0, 1, 1, 1)
;~ 		MsgBox(64,'found 1',$found&' - '&$entry)
		If $found > -1 Then
			_ArrayDelete($aArray, $found)
			$entry -= 1
			ContinueLoop
		EndIf

		$found = _ArraySearch($aArray,$nameIP[$entry], 0, 0, 0, 1, 1, 2)
;~ 		MsgBox(64,'found 2',$found&' - '&$entry)
		If $found > -1 Then
			_ArrayDelete($aArray, $found)
			$entry -= 1
			ContinueLoop
		EndIf

	Next

#cs
	For $entry In $nameIP

		$found = _ArraySearch($aArray,$entry, 0, 0, 0, 1, 1, 1)
;~ 		MsgBox(64,'found 1',$found&' - '&$entry)
		If $found > -1 Then _ArrayDelete($aArray, $found)

		$found = _ArraySearch($aArray,$entry, 0, 0, 0, 1, 1, 2)
;~ 		MsgBox(64,'found 2',$found&' - '&$entry)
		If $found > -1 Then _ArrayDelete($aArray, $found)

	Next
	#ce
;~ 	_ArrayDisplay($aArray , '$aArray')

	Return $aArray

EndFunc


#EndRegion Scan ##########################################################################

#Region Modify PoolParts ###############################################################
Func _CallPool7()
	If MsgBox(36,'Modify Device Pools', 'Are you sure you want to modify device pools?') = 6 Then _Modify_Device_Pool('7', $aMethod[13][0], $CUCM7, GUICtrlRead($Combo_Device_Pools_7, 1), 'devicePoolName')
EndFunc

Func _CallPool10()
	If MsgBox(36,'Modify Device Pools', 'Are you sure you want to modify device pools?') = 6 Then _Modify_Device_Pool('10', $aMethod[13][1], $CUCM10, GUICtrlRead($Combo_Device_Pools_10, 1), 'devicePoolName')
EndFunc

Func _Modify_Device_Pool($version, $method, $idListview, $devicePool, $PhoneUpdateParameter)
	Local	$aXML, $return, $returnParsed, $iRow, $1st,  $device, _
			$LogFile = $LogFolder&'\CUCM '&$version&' Device Pool Mods.log', $checkedbox

	$1st = _GUICtrlListview_GetItemText($idListview, 0, 1)

	If $devicePool = 'Device Pools' Then
		MsgBox(48,'No Selection', 'Please select a device pool from the selected device pool first.')
		Return
	ElseIf $1st = 'No Devices Found' Or $1st = '' Then
		MsgBox(48,'No Data', 'There are no phones listed')
		Return
	EndIf

	$iRow = _GUICtrlListView_GetItemCount($idListview)

	For	$iNdex = 0 To $irow - 1

		$device = _GUICtrlListview_GetItemText($idListview, $iNdex, 1)

		;Validate check box status
		$checkedbox = _GUICtrlListView_GetItemChecked($idListview, $iNdex)
		If $checkedbox Then
			_Status('Updating Device Pool for '&$device, $version)

			$return = _Call($version, $method, $device, $devicePool, $PhoneUpdateParameter)
			$returnParsed = _Find($return, 'return')

			If $returnParsed = 'Error' Or $returnParsed = 'Failed' Then
				MsgBox(16,'Device Pool Error', 'There was an error in the return XML for '&$device&'. The XML return string has been logged.')
				_Log($LogFile, $device&','&$returnParsed)
				_Status($device&','&$returnParsed, $version)
			Else
				_Log($LogFile, $device&','&$returnParsed)
				_Status($device&','&$returnParsed, $version)
			EndIf

			_Status($device&','&$returnParsed, $version)
		Else
			_Log($LogFile, $device&', Device Skipped due to list checkbox exception.')
			_Status($device&', Device Skipped due to list checkbox exception.', $version)
		EndIf

	Next

	_Status('Device Pool Modifications are complete.', $version)

	MsgBox(64,'Complete','Device Pool Modifications are complete.')

EndFunc

Func _CallPart7()
	Local	$try = 'stay'
	If BitAND(GUICtrlRead($CheckBox_Partition10), $GUI_CHECKED) Then
		If MsgBox(36,'Modify Partitions', 'Are you sure you want to modify device partitions for both CUCM 7 & CUCM 10?') = 6 Then
			$try =  _Modify_Partitoins('7', $aMethod[3][0], $aMethod[14][0], $CUCM7, GUICtrlRead($Combo_Partitions_7, 1), 'uuid')
			If $try = 'no go' Then Return
			MsgBox(64,'Complete 7 Modificatoin','Line partition modifications for CUCM 7 are complete.'&@CRLF&'Please wait for CUCM 10 partition modifications')
			_Modify_Partitoins('10', $aMethod[3][1], $aMethod[14][1], $CUCM7, GUICtrlRead($Combo_Partitions_10_From_7, 1), 'uuid')
			MsgBox(64,'Complete 10','All line partition modifications are complete.')
		EndIf
	ElseIf MsgBox(36,'Modify Partitions', 'Are you sure you want to modify device partitions for CUCM 7?') = 6 Then
		$try = _Modify_Partitoins('7', $aMethod[3][0], $aMethod[14][0], $CUCM7, GUICtrlRead($Combo_Partitions_7, 1), 'uuid')
;~ 		MsgBox(64,'$try', $try)
		If $try <> String('no go') Then
			MsgBox(64,'Complete','All line partition modifications are complete.')
		EndIf
	EndIf

EndFunc

Func _CallPart10()
	If MsgBox(36,'Modify Partitions', 'Are you sure you want to modify device partitions?') = 6 Then _Modify_Partitoins('10', $aMethod[3][1], $aMethod[14][1], $CUCM10, GUICtrlRead($Combo_Partitions_10, 1), 'uuid')
EndFunc

Func _Modify_Partitoins($version, $method1, $method2, $idListview,  $partition, $typeMethod)
	Local	$aXML, $return, $returnParsed, $iRow, $1st,  $device, $LogFile = $LogFolder&'\CUCM '&$version&' Partition Mods.log', _
			$aLines, $checkedbox, $UUIDerror = False, $partError = False, $patternReturn


	$1st = _GUICtrlListview_GetItemText($idListview, 0, 1)

	If $partition = 'Partitions' Then
		MsgBox(48,'No Selection', 'Please select a partition from the partition list first.')
		Return 'no go'
	ElseIf $1st = 'No Devices Found' Or $1st = '' Then
		MsgBox(48,'No Data', 'There are no phones listed')
		Return 'no go'
	ElseIf $version = '7' And BitAND(GUICtrlRead($CheckBox_Pattern_7), $GUI_CHECKED) = 1 Then
		If GUICtrlRead($Combo_Patterns_7, 1) = "Pattern Partition" Then
			MsgBox(48, 'Error', "Please select a pattern partition first.")
			_Status($device&', Device Skipped due no pattern partition set.', $version)
			Return 'no go'
		EndIf
	ElseIf $version = '7' And BitAND(GUICtrlRead($CheckBox_Partition10), $GUI_CHECKED) = 1 Then
		If GUICtrlRead($Combo_Partitions_10_From_7, 1) = "10.5 Partition" Then
			MsgBox(48, 'Error', "Please select a CUCM 10 partition first.")
			_Status($device&', Device Skipped due no pattern partition set.', $version)
			Return 'no go'
		EndIf
	EndIf

	$iRow = _GUICtrlListView_GetItemCount($idListview)

	For	$iNdex = 0 To $irow - 1

		$device = _GUICtrlListview_GetItemText($idListview, $iNdex, 1)

		;Validate Checkbox status
		$checkedbox = _GUICtrlListView_GetItemChecked($idListview, $iNdex)
		If $checkedbox Then
			$aLines = ''
			$return = ''
			_Status('Retrieving line UUIDs for '&$device, $version)

			;Get Lines From Phone
			$return = _Call($version, $method1, $device)

			$aLines = _Find($return, 'Line UUIDs')

			If $aLines = 'Failed' Or Not IsArray($aLines) Then
				_Log($LogFile, $device&','&$return)
				_Status($device&','&$return, $version)
;~ 				MsgBox(16,'Partition Error', 'There was an error in the return XML for '&$device&"'s Lines. The XML return string has been logged.")
				$UUIDerror = True

			EndIf

	;~ 		_ArrayDisplay($aLines, 'Lines for '&$device)

			_Status('Updating Partitions for lines on '&$device, $version)
			If Not $UUIDerror = True Then
				For $UUID In $aLines
					;Update Partition of line based on UUID
					$return = _Call($version, $method2, $UUID, $partition, $typeMethod)


					$returnParsed = _Find($return, 'return')

					If $returnParsed = 'Error' Or $returnParsed = 'Failed' Then
						_Log($LogFile, $device&','&$returnParsed&','&$return)
						_Status($device&','&$returnParsed, $version)
						$partError = True
					Else
						_Log($LogFile, $device&','&$returnParsed)
						_Status( $device&','&$returnParsed, $version)
					EndIf

					_Status($UUID&' | '&$returnParsed, $version)

					 ;BitAnd(GuiCtrlRead(?), $GUI_CHECKED)  0 = Unchecked | 1 = Checked
					If $version = '7' And BitAND(GUICtrlRead($CheckBox_Pattern_7), $GUI_CHECKED) = 1 Then
						If GUICtrlRead($Combo_Patterns_7, 1) = "Pattern Partition" Then
							MsgBox(48, 'Error', "Please select a pattern partition first.")
							_Log($LogFile, $device&' ,Device Skipped due to no pattern partition set.')
							_Status($device&', Device Skipped due to no pattern partition set.', $version)

						Else
							$patternReturn = _PatternAdd($UUID, $version, $device, GUICtrlRead($Edit_Pattern_Desc_7, 1))
						EndIf
					EndIf

				Next
			EndIf
		Else
			_Log($LogFile, $device&', Device Skipped due to list checkbox exception.')
			_Status($device&', Device Skipped due to list checkbox exception.', $version)
		EndIf

	Next

	If $UUIDerror = True Then
		MsgBox(16,'UUID Error', 'There was an error in the return XMLs for CUCM '&$version&' UUID retrieval. Please check the log for additional information.')
		$UUIDerror = False
	EndIf

	If $patternReturn = True Then
		MsgBox(16,'Pattern Error', 'There was an error in the return XMLs for CUCM 10 pattern change. Please check the log for additional information.')
		$patternReturn = False
	EndIf

	If $partError = True Then
		MsgBox(16,'Partition Error', 'There was an error in the return XMLs for CUCM '&$version&' partition change. Please check the log for additional information.')
		$partError = False
	EndIf

	_Status('Line Partition Modifications are complete.', $version)
	Return 'all good'

EndFunc

Func _PatternAdd($UUID, $version, $device, $Desc = 'UCM10')
	Local	$return, $apattern, $aParsed, $LogFile = $LogFolder&'\Add Pattern.log'

	_Status('Updating Patterns for lines on '&$device, $version)

	;Check Description
	If $Desc <> 'UCM10' Then $Desc &= ' - UCM10'

	;Get Pattern from Line
	$return = _Call($version, $aMethod[6][0], $UUID, '', 'uuid')

	$aParsed = _Find($return, 'Line Pattern')

	If $aParsed = 'Error' Or $aParsed = 'Failed' Or Not IsArray($aParsed) Then
		_Log($LogFile, $device&','&$aParsed)
		_Status( $device&','&$aParsed, $version)
;~ 		MsgBox(16,'Line Error', 'There was an error in the return XMLs for CUCM 10. Please check the log for additional information.')
		Return True
	EndIf

	$return = ''

	;Add Pattern
	$return = _Call($version, $aMethod[7][0], $aParsed[0],'','',$Desc)

	;Validate return was successful (not array)
	$apattern = _Find($return, 'return')

	If $apattern = 'Exist' Then
		_Log($LogFile, $device&','&$aParsed[0])
		_Status($device&','&$aParsed[0], $version)
	ElseIf $apattern =  'Error' Or $aParsed[0] = 'Failed' Then
		_Log($LogFile, $device&','&$aParsed[0])
		_Status($device&','&$aParsed[0], $version)
;~ 		MsgBox(16,'Pattern Error', 'There was an error in the return XML for '&$aParsed[0]&". The XML return string has been logged.")
		Return True
	Else
		_Log($LogFile, $device&','&$aParsed[0])
		_Status($device&','&$aParsed[0], $version)
	EndIf

EndFunc

Func _CallPart10from7()

	_Modify_Partitoins('10', $aMethod[3][1], $aMethod[14][1], $CUCM7, 'voip-5digit', 1)

EndFunc

Func _CallCheckBox7()

	_CheckBox('7 Pattern')

EndFunc

Func _CheckBox_Partition10()

	_CheckBox('7-10 Partition')

EndFunc

Func _CallCheckBox10()

	_CheckBox('10 Pattern')

EndFunc

Func _CheckBox($version)
	;0 = Unchecked | 1 = Checked
	Switch $version
		Case	'7 Pattern'
			Switch	BitAND(GUICtrlRead($CheckBox_Pattern_7), $GUI_CHECKED)
				Case 0
					GUICtrlSetState($Combo_Patterns_7, $GUI_DISABLE)
					GUICtrlSetState($Edit_Pattern_Desc_7, $GUI_DISABLE)
				Case 1
					GUICtrlSetState($Combo_Patterns_7, $GUI_ENABLE)
					GUICtrlSetState($Edit_Pattern_Desc_7, $GUI_ENABLE)
			EndSwitch
		Case	'10 Pattern'
			Switch	BitAND(GUICtrlRead($CheckBox_Pattern_10), $GUI_CHECKED)
				Case 0
					GUICtrlSetState($Combo_Patterns_10, $GUI_DISABLE)
					GUICtrlSetState($Edit_Pattern_Desc_10, $GUI_DISABLE)
				Case 1
					GUICtrlSetState($Combo_Patterns_10, $GUI_ENABLE)
					GUICtrlSetState($Edit_Pattern_Desc_10, $GUI_ENABLE)
			EndSwitch
		Case	'7-10 Partition'
			Switch	BitAND(GUICtrlRead($CheckBox_Partition10), $GUI_CHECKED)
				Case 0
					GUICtrlSetState($Combo_Partitions_10_From_7, $GUI_DISABLE)
				Case 1
					GUICtrlSetState($Combo_Partitions_10_From_7, $GUI_ENABLE)
			EndSwitch
	EndSwitch

EndFunc

#EndRegion CUCM PoolParts ##############################################################

#Region Phone Reset ####################################################################
Func _CallReset7()
	If MsgBox(36,'Reset Phones', 'Are you sure you want to reset phones?') = 6 Then _Reset('7', $aMethod[15][0], $CUCM7)
EndFunc

Func _CallReset10()
	If MsgBox(36,'Reset Phones', 'Are you sure you want to reset phones?') = 6 Then _Reset('10', $aMethod[15][1], $CUCM10)
EndFunc

Func _Reset($version, $method, $idListview)

	Local	$aXML, $return, $returnParsed, $iRow, $1st,  $device, _
			$LogFile = $LogFolder&'\CUCM '&$version&' Phone Resets.log', $checkedbox

	$1st = _GUICtrlListview_GetItemText($idListview, 0, 1)

	If $1st = 'No Devices Found' Or $1st = '' Then
		MsgBox(48,'No Data', 'There are no phones listed to reset')
		Return
	EndIf

	$iRow = _GUICtrlListView_GetItemCount($idListview)

	For	$iNdex = 0 To $irow - 1

		$device = _GUICtrlListview_GetItemText($idListview, $iNdex, 1)

		$checkedbox = _GUICtrlListView_GetItemChecked($idListview, $iNdex)
		If $checkedbox Then
			_Status('Resetting phone '&$device, $version)

			$return = _Call($version, $method, $device)

			$returnParsed = _Find($return, 'return')

			If $returnParsed = 'Error' Or $returnParsed = 'Failed' Then
				MsgBox(16,'Error', 'There was an error in the return XML for '&$device&'. The XML return string has been logged.')
			Else
				_Log($LogFile, $device&','&$returnParsed)
				_Status( $device&','&$returnParsed, $version)
			EndIf

			_Status($device&','&$returnParsed, $version)
		Else
			_Log($LogFile, $device&', Device Skipped due to list checkbox exception.')
			_Status($device&', Device Skipped due to list checkbox exception.', $version)
		EndIf



	Next

	_Status('Phone Resets are complete.', $version)

	MsgBox(64,'Complete','Phone Resets are complete.')

EndFunc

#EndRegion Phone Reset#######################################################################

#Region Call CallManager ###############################################################

Func _Call($version, $method, $value, $value2 = '', $method_Specific = '', $Desc = '')
	Local	$aXML, $return

	$aXML = _XML_Formatter($version, $method, $value, $value2, $method_Specific, $Desc)

	$return = _SOAP_Open($aXML[0], $aXML[1], $aXML[2], $aXML[3], $aXML[4], $aXML[5])

	Return $return

EndFunc


Func _Call_RIS($version, $method, $value, $status = '', $searchParam = '')
	Local	$aXML, $return

	$aXML = _XML_FormatterRIS($version, $method, $value,$status, $searchParam)
;~ 	_ArrayDisplay($aXML, 'aXML 2')

	$return = _SOAP_Open($aXML[0], $aXML[1], $aXML[2], $aXML[3], $aXML[4], $aXML[5])

	Return $return

EndFunc
#EndRegion Call CallManager RISPort ####################################################

#Region Call Logs ######################################################################

Func _CallLogFolder()

ShellExecute($LogFolder, '', $LogFolder, 'open')

EndFunc


#EndRegion Call Logs####################################################################

#Region Undo ###########################################################################

Func _CallUndo7()
	If MsgBox(36,'Modify Partitions', 'Are you sure you want to Undo 10 Conversions?') = 6 Then _Undo('10', $aMethod[3][1], $aMethod[14][1], $CUCM10, GUICtrlRead($Combo_Partitions_10, 1), 'uuid')
EndFunc

Func _CallUndo10()
	If MsgBox(36,'Modify Partitions', 'Are you sure you want to Undo 7 Conversions?') = 6 Then _Undo('10', $aMethod[3][1], $aMethod[14][1], $CUCM10, GUICtrlRead($Combo_Partitions_10, 1), 'uuid')
EndFunc

Func _Undo($version, $method1, $method2, $idListview,  $partition, $typeMethod)
	Local	$return, $returnParsed, $iRow, $1st,  $device, $LogFile = $LogFolder&'\CUCM '&$version&' Partition Mods.log', _
			$aLines, $checkedbox, $UUIDerror = False, $partError = False, $patternReturn
	$iRow = _GUICtrlListView_GetItemCount($idListview)

	For	$iNdex = 0 To $irow - 1

		$device = _GUICtrlListview_GetItemText($idListview, $iNdex, 1)

		;Validate check box status
		$checkedbox = _GUICtrlListView_GetItemChecked($idListview, $iNdex)
		If $checkedbox Then
			_Status('Updating Device Pool for '&$device, $version)

			;$return = _Call($version, $method1, $device, $devicePool, $PhoneUpdateParameter)
			$returnParsed = _Find($return, 'return')

			If $returnParsed = 'Error' Or $returnParsed = 'Failed' Then
				MsgBox(16,'Device Pool Error', 'There was an error in the return XML for '&$device&'. The XML return string has been logged.')
				_Log($LogFile, $device&','&$returnParsed)
				_Status($device&','&$returnParsed, $version)
			Else
				_Log($LogFile, $device&','&$returnParsed)
				_Status($device&','&$returnParsed, $version)
			EndIf

			_Status($device&','&$returnParsed, $version)
		Else
			_Log($LogFile, $device&', Device Skipped due to list checkbox exception.')
			_Status($device&', Device Skipped due to list checkbox exception.', $version)
		EndIf

	Next

EndFunc

#EndRegion Undo#########################################################################

#Region Misc ###########################################################################

Func _CallCheckAll7()

	_CheckALL($CheckBox_All_7, $CUCM7)

EndFunc

Func _CallCheckAll10()

	_CheckALL($CheckBox_All_10, $CUCM10)

EndFunc

Func _CheckALL($CheckBox, $idListview)
	Local	$CheckStatus, $irow

	$CheckStatus = BitAND(GUICtrlRead($CheckBox), $GUI_CHECKED)

	$irow = _GUICtrlListView_GetItemCount($idListview)

	If $CheckStatus = 0 Then
		For $x = 0 To $irow - 1
			_GUICtrlListView_SetItemChecked($idListview, $x, False)
		Next
	Else
		For $x = 0 To $irow - 1
			_GUICtrlListView_SetItemChecked($idListview, $x)
		Next
	EndIf

EndFunc

Func _CallSort7()

	_Sort($CUCM7)

EndFunc

Func _CallSort10()

	_Sort($CUCM10)

EndFunc

Func _Sort($idListview)

	_GUICtrlListView_SortItems($idListview, GUICtrlGetState($idListview))

EndFunc

;~ Find used to search Pools and Partitions only
Func _Find($input, $type)
	Local	$aParsed[1], $LogFile, $ascanCol = 5
;~ 	MsgBox(64,'Here', $type)

	Switch $type
		Case 'scanner'
				Local	$LogFile, $aName, $aIP, $aStatus, $aDescription

				ReDim $aParsed[1][$ascanCol]
				$LogFile = $LogFolder&'\IP Scan.log'
				If StringInStr($input, '<faultcode>') Then
					_Log($LogFile, 'Error, Faultcode : '&$input)
					Return 'Failed'
				EndIf

				$aName = _StringBetween($input, '<item xsi:type="ns1:CmDevice"><Name xsi:type="xsd:string">', '</Name>')
				If Not IsArray($aName) Then
					_Log($LogFile,'Error, Name Not Found : '&$input)
					Return
				EndIf

				$aIP = _StringBetween($input, '</Name><IpAddress xsi:type="xsd:string">', "</IpAddress>")
				If Not IsArray($aIP) Then
					_Log($LogFile, 'Error,Status Not Found: '&$input)
					Return
				EndIf

				$aStatus = _StringBetween($input, '<Status xsi:type="ns1:CmDevRegStat">', '</Status>')
				If Not IsArray($aStatus) Then
					_Log($LogFile, 'Error, IP Not Found : '&$input)
					Return
				EndIf

				$aDescription =  _StringBetween($input, '</DChannel><Description xsi:type="xsd:string">', '</Description>')
				If Not IsArray($aDescription) Then
					;It's ok
			;~ 			FileWriteLine('Error, IP Not Found : '&$input &@CRLF)
			;~ 			Return
				EndIf


			;~ 	_ArrayDisplay($aName)

				ReDim $aParsed[UBound($aName)][$ascanCol]

			;~ 	_ArrayDisplay($aParsed, 'Before')

				For $x = 0 To UBound($aName) - 1
					$aParsed[$x][0] = $x + 1
					$aParsed[$x][1] = $aName[$x]
					$aParsed[$x][2] = $aIP[$x]
					$aParsed[$x][3] = $aStatus[$x]
					$aParsed[$x][4] = $aDescription[$x]
				Next

				Return $aParsed

		Case 'poolparts'
				$LogFile = $LogFolder&'\***Device Pool and Partitions List***.log'

				If StringInStr($input, '<faultcode>') Then
					_Log($LogFile, 'Error, Faultcode : '&$input)
					Return 'Failed'
				EndIf

				$aParsed = _StringBetween($input, '<name>', '</name>')
				If Not IsArray($aParsed) Then
					_Log($LogFile, 'Error, Name Not Found : '&$input)
					Return
				EndIf

				If Not IsArray($aParsed) Then
					_Log($LogFile, 'Error, Unknown : '&$input)
					Return 'Error'
				EndIf

				For $parse In $aParsed
					_Log($LogFile, 'Found,'&$parse)
				Next

				_ArraySort($aParsed)

				Return $aParsed
		Case 'return'

			If StringInStr($input, '<faultcode>') Then
				Return 'Failed'
			EndIf

			$aParsed = _StringBetween($input, "<return>", "</return>")

			Return $aParsed[0]
		Case 'Line UUIDs'
			If StringInStr($input, '<faultcode>') Then
				Return 'Failed'
			EndIf

			$aParsed = _StringBetween($input, '<dirn uuid="{', '}"')

			Return $aParsed

		Case 'Line Pattern'
			If StringInStr($input, '<faultcode>') Then
				If StringInStr($input, 'A Route Pattern exists') Then
					Return 'Exist'
				Else
					Return 'Failed'
				EndIf
			EndIf



			$aParsed = _StringBetween($input, '<pattern>', '</pattern>')

			Return $aParsed

		Case Else
			MsgBox(16,'Error', 'No search parameters found for find funcion.')
			Return 'Error'
	EndSwitch

EndFunc

;~ Clear screens
Func _Clear()

	_GUICtrlListView_DeleteAllItems($CUCM7)
	_GUICtrlListView_DeleteAllItems($CUCM10)

EndFunc

Func _Export()

	Local	$sFile, $aSaveText1, $iColumnNdex_7, $iColumnNdex_10, $iRowNdex_7, $iRowNdex_10, _
			$colHeader

;~ 	GUISetState(@SW_HIDE, $Export_GUI)

;~ 	_Log("Exporting Inoformation")

	$iColumnNdex_7 = _GUICtrlListView_GetColumnCount($CUCM7)
	$iRowNdex_7 = _GUICtrlListView_GetItemCount($CUCM7)

	$iColumnNdex_10 = _GUICtrlListView_GetColumnCount($CUCM10)
	$iRowNdex_10 = _GUICtrlListView_GetItemCount($CUCM10)

	If $iRowNdex_7 And $iRowNdex_10 = 0 Then
		MsgBox(16,'No Data', "Error, there's no data to save")
		Return
	EndIf

	If BitAND(GUICtrlRead($Radio_7), $GUI_CHECKED) = $GUI_CHECKED Then

		If $iRowNdex_7  = 0 Then
			MsgBox(16,'No Data', "Error, there's no data to save")
			Return
		EndIf

;~ 		_Log("Creating Add File")

;~ 		$aSaveText1 = _GUICtrlListView_GetItemTextArray($CUCM7)


		Dim $aSaveText1[$iRowNdex_7 + 1][$iColumnNdex_7]

		For $j = 0 To $iColumnNdex_7 - 1

			$colHeader = _GUICtrlListView_GetColumn($CUCM7,$j)
			If $j = 0 Then
				$aSaveText1[0][$j] = 'Index'
			Else
				$aSaveText1[0][$j] = $colHeader[5]
			EndIf

		Next

		For $k = 0 To $iRowNdex_7 - 1

			For $j = 0 To $iColumnNdex_7 - 1
				$aSaveText1[$k + 1][$j] = _GUICtrlListView_GetItemText($CUCM7, $k, $j)
			Next

		Next

;~ 		MsgBox(64,'Ubound', "Dim: "&UBound($aSaveText1, 2)&@CRLF&"Index: "&UBound($aSaveText1))

		If Not IsArray($aSaveText1) Or $aSaveText1[0][0] = '0' Then
			MsgBox(16, "Canceled", "There's no data to save.")
			_Close_Export()
			Return "No Save"
		EndIf

		$sFile = FileSaveDialog("Export", @ScriptDir, "Text (csv.*;txt.*)", 18, "CUCM Migrator Export", $Export_GUI)

		_Write($aSaveText1, '7', $sFile)

	ElseIf BitAND(GUICtrlRead($Radio_10), $GUI_CHECKED) = $GUI_CHECKED Then

		If $iRowNdex_10  = 0 Then
			MsgBox(16,'No Data', "Error, there's no data to save")
			Return
		EndIf

;~ 		_Log("Creating Delete File")

		Dim $aSaveText1[$iRowNdex_10 + 1][$iColumnNdex_10]

		For $j = 0 To $iColumnNdex_10 - 1

			$colHeader = _GUICtrlListView_GetColumn($CUCM10,$j)
			If	$j = 0 Then
				$aSaveText1[0][$j] = 'Index'
			Else
				$aSaveText1[0][$j] = $colHeader[5]
			EndIf

		Next

		For $k = 0 To $iRowNdex_10 - 1

			For $j = 0 To $iColumnNdex_10 - 1
				$aSaveText1[$k + 1][$j] = _GUICtrlListView_GetItemText($CUCM10, $k, $j)
			Next

		Next

;~ 		MsgBox(64,'Ubound', "Dim: "&UBound($aSaveText1, 2)&@CRLF&"Index: "&UBound($aSaveText1))

		If Not IsArray($aSaveText1) Or $aSaveText1[0][0] = '0' Then
			MsgBox(16, "Canceled", "There's no data to save.")
			_Close_Export()
			Return "No Save"
		Else
			$sFile = FileSaveDialog("Export", @ScriptDir, "Text (csv.*;txt.*)", 18, "CUCM Migrator Export", $Export_GUI)
			_Write($aSaveText1, '10', $sFile)
		EndIf



	ElseIf BitAND(GUICtrlRead($Radio_Both), $GUI_CHECKED) = $GUI_CHECKED Then
		If $iRowNdex_7  = 0 Then
			MsgBox(16,'No Data', "Error, there's no data to save in grid 7")
		Else

	;~ 		_Log("Creating ADD & Delete File")

			Dim $aSaveText1[$iRowNdex_7 + 1][$iColumnNdex_7]

			For $j = 0 To $iColumnNdex_7 - 1

				$colHeader = _GUICtrlListView_GetColumn($CUCM7,$j)
				If $j = 0 Then
					$aSaveText1[0][$j] = 'Index'
				Else
					$aSaveText1[0][$j] = $colHeader[5]
				EndIf

			Next

			For $k = 0 To $iRowNdex_7 - 1

				For $j = 0 To $iColumnNdex_7 - 1
					$aSaveText1[$k + 1][$j] = _GUICtrlListView_GetItemText($CUCM7, $k, $j)
				Next

			Next

	;~ 		MsgBox(64,'Ubound', "Dim: "&UBound($aSaveText1, 2)&@CRLF&"Index: "&UBound($aSaveText1))

			If Not IsArray($aSaveText1) Or $aSaveText1[0][0] = '0' Then
				MsgBox(16, "Canceled", "There's no data in table 1 to save.")
				_Close_Export()
				Return "No Save"
			Else
				$sFile = FileSaveDialog("Export", @ScriptDir, "Folder (*.*)", 18, "CUCM Migrator Export", $Export_GUI)

				_Write($aSaveText1, '7', $sFile)

			EndIf
		EndIf

;~ 		=====================================================================

		If $iRowNdex_7  = 0 Then
			MsgBox(16,'No Data', "Error, there's no data to save in grid 10")
			Return
		Else


			Dim $aSaveText1[$iRowNdex_10 + 1][$iColumnNdex_10]

			For $j = 0 To $iColumnNdex_10 - 1

				$colHeader = _GUICtrlListView_GetColumn($CUCM10,$j)
				If $j = 0 Then
					$aSaveText1[0][$j] = 'Index'
				Else
					$aSaveText1[0][$j] = $colHeader[5]
				EndIf

			Next

			For $k = 0 To $iRowNdex_10 - 1

				For $j = 0 To $iColumnNdex_10 - 1
					$aSaveText1[$k + 1][$j] = _GUICtrlListView_GetItemText($CUCM10, $k, $j)
				Next

			Next

	;~ 		MsgBox(64,'Ubound', "Dim: "&UBound($aSaveText1, 2)&@CRLF&"Index: "&UBound($aSaveText1))

			If Not IsArray($aSaveText1)  Or $aSaveText1[0][0] = '0' Then
				MsgBox(16, "Canceled", "There's no data in table 2 to save.")
				_Close_Export()
				Return "No Save"
			Else
				_Write($aSaveText1, '10', $sFile)
			EndIf
		EndIf
	EndIf

	$aSaveText1 = ''

	MsgBox(64,'Complete', 'The export process is complete.')

	_Close_Export()

EndFunc

Func _Write($output, $ver, $location = '')
	Local	$aParsed

	_FileWriteFromArray($location&"_"&$ver&".csv", $output, Default, Default, ',')

;~ 	_Log($ver, "Saving: "&$location&"_"&$ver&".csv")

EndFunc

Func _Rem_WhiteSpace()
	Local	$sXcept, $sMport, $aXcept, $aMport

	$sMport = GUICtrlRead($ListListview, 1)
	$sXcept = GUICtrlRead($ExceptionListview, 1)

	$aMport = _StringExplode($sMport, @CR)
	$aXcept = _StringExplode($sXcept, @CR)

	$sMport = ''
	$sXcept = ''

	GUICtrlSetData($ListListview, '')
	GUICtrlSetData($ExceptionListview, '')

	For $AM = 0 To UBound($aMport) - 1
		If $AM = UBound($aMport) - 1 Then
			$sMport &= StringStripWS($aMport[$AM], 8)
		Else
			$sMport &= StringStripWS($aMport[$AM], 8)&@CRLF
		EndIf
	Next

	For $AX = 0 To UBound($aXcept) - 1
		If $AX = UBound($aXcept) - 1 Then
			$sXcept &= StringStripWS($aXcept[$AX], 8)
		Else
			$sXcept &= StringStripWS($aXcept[$AX], 8)&@CRLF
		EndIf
	Next

	GUICtrlSetData($ListListview, StringReplace($sMport, ' ', ''))
	GUICtrlSetData($ExceptionListview, StringReplace($sXcept, ' ', ''))

;~	MsgBox(64,'Test','Test')


#cs

	MsgBox(64,'iMport', StringLen($iMport) & ' - ' & $iMport)
	MsgBox(64,'eXcept', StringLen($eXcept) & ' - ' & $eXcept)

	If $iMport <> "" Then GUICtrlSetData($ListListview, StringReplace($iMport, Chr(32), ''))
	If $iMport <> "" Then GUICtrlSetData($ListListview, StringStripWS($iMport, 7))
	If $eXcept <> "" Then GUICtrlSetData($ExceptionListview, StringReplace($eXcept, Chr(32), ''))
	If $eXcept <> "" Then GUICtrlSetData($ExceptionListview, StringStripWS($eXcept, 7))

	$iMport = GUICtrlRead($ListListview, 1)
	$eXcept = GUICtrlRead($ExceptionListview, 1)
	MsgBox(64,'iMport', StringLen($iMport) & ' - ' & $iMport)
	MsgBox(64,'eXcept', StringLen($eXcept) & ' - ' & $eXcept)
#ce
EndFunc

;Lock Scanning
;===============================================================================
; Name:   _Lock()
; Description:   Lock Controls in GUI
; Syntax:   _Lock($status)
; Parameter(s):   $status = $GUI_DISABLE  or $GUI_ENABLE
; Requirement(s):   $status
; Return Value(s):   -
; Author(s)   Dominique J. Thornton
; Note(s):	These controls will be locked while running specific functions
;			Buttons
;				$Button_Scan7, $Button_Scan10, $Button_All, $Button_Reset7, $Button_Reset_10
;			IP Params
;				$IPAddress_CUCM7, $IPAddress_CUCM10
; Example(s):
;===============================================================================
Func _Lock($status)

	GUICtrlSetState($Button_Scan7, $status )
	GUICtrlSetState($Button_Scan10, $status )
	GUICtrlSetState($Button_All, $status )
	GUICtrlSetState($Button_Reset7, $status )
	GUICtrlSetState($Button_Reset_10, $status )
	GUICtrlSetState($IPAddress_CUCM7, $status )
	GUICtrlSetState($IPAddress_CUCM10, $status )

EndFunc

;Statusbar updates
Func _Status($verbiage, $ver)
	Local	$LogFile = $LogFolder&'\Status.log'
	Switch $ver
		Case '7'
				ConsoleWrite($verbiage&@CRLF)
				_GUICtrlStatusBar_SetText($StatusBar, '7: ' & $verbiage, 1)
		Case '10'
				ConsoleWrite($verbiage&@CRLF)
				_GUICtrlStatusBar_SetText($StatusBar, '10: ' & $verbiage, 2)
		Case	Else
			ConsoleWrite($verbiage&@CRLF)
			_GUICtrlStatusBar_SetText($StatusBar, '7: ' & $verbiage, 1)
			_GUICtrlStatusBar_SetText($StatusBar, '10: ' & $verbiage, 2)
	EndSwitch

	_Log($LogFile, $verbiage)

EndFunc

;~ Log info
Func _Log($Log, $verbiage)
	Local	$stamp

		$stamp = '['&@MON&'/'&@MDAY&'/'&@YEAR& ' '&@HOUR&':'&@MIN&':'&@SEC&'] ,'
		FileWriteLine($Log, $stamp&$verbiage&@CRLF)

	Return
EndFunc

Func _XSkinGroup($Text, $Pleft, $Ptop, $Pwidth, $Pheight, $color)
	Local $XS_n, $PControl
	If StringInStr(@OSType, "WIN32_NT") Then
		$XS_n = DllCall("uxtheme.dll", "int", "GetThemeAppProperties")
		DllCall("uxtheme.dll", "none", "SetThemeAppProperties", "int", 0)
	EndIf
	$PControl = GUICtrlCreateGroup($Text, $Pleft, $Ptop, $Pwidth, $Pheight)
	;GUICtrlSetBkColor($PControl, $color)
	GUICtrlSetColor($PControl, $color)
	If StringInStr(@OSType, "WIN32_NT") And IsArray($XS_n) Then
		DllCall("uxtheme.dll", "none", "SetThemeAppProperties", "int", $XS_n[0])
	EndIf
	Return $PControl
EndFunc   ;==>_XSkinGroup

Func _Exit_IP_Exceptions()
	GUISetState(@SW_HIDE, $ExceptionList)
EndFunc

Func _Exit_IP_List()
	GUISetState(@SW_HIDE, $ListGUI)
EndFunc

Func _Close_Export()

	GUIDelete($Export_GUI)

EndFunc

;~ Exit syste
Func _Exit()

	For $x = 255 To 0 Step -5
		WinSetTrans($CUCM_Migrator, '', $x)
		Sleep(1)
	Next

	_GUICtrlListView_UnRegisterSortCallBack($CUCM7)
	_GUICtrlListView_UnRegisterSortCallBack($CUCM10)

	GUIDelete($CUCM_Migrator)
	Exit

EndFunc
#EndRegion Misc#######################################################################







