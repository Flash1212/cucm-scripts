#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile=Post to CUCM x86.exe
#AutoIt3Wrapper_Outfile_x64=Post to CUCM x64.exe
#AutoIt3Wrapper_Compile_Both=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****


;Gui Includes
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <GuiStatusBar.au3>
#include <StaticConstants.au3>
#include <TabConstants.au3>
#include <WindowsConstants.au3>
#include <GuiListView.au3>
#include <WinAPITheme.au3>
#include <GuiComboBox.au3>
#include <ComboConstants.au3>

;Function Includes
#include <File.au3>
#include <Array.au3>
#include <String.au3>
#include <WinHTTP.au3>
#include <.\soap_xml.au3>
#include <.\SOAP_Open.au3>

;~ #include <.\PrettyXML.au3>

Opt('GUIOnEventMode', 1)
Opt('MustDeclareVars', 1)

$SHOW1 = True
$SHOW2 = False

Global	$oMyError

;Misc Variables

Global	$action, $item, $progress,  $ColumnInsertPoint = 0, $Highest_Line_Count = 0, $Highest_Speedial_Count = 0, $Highest_BusyLamp_Count = 0, _
		$Line_Start = 0, $Speed_Start = 0, $BusyLamp_Start = 0, $Skip = False, $1st = True, $ADD_Dimensions, _
		$Both = False, $BackgroundSave = False, $aTempListView[1][1], $Current_DP = 'All Device Pools', $BuildSave = False

;Object Variables
Global 	$objHTTP, $objXMLSend, $objReturn, _
		$strReturn, $strQuery, $returnXML, $returnText, _
		$version, $method
#cs Moved into soap_xml.au3
;Methods list
Global $aMethod[12][2]

	;7.x
	$aMethod[0][0] = 'listDevicePoolByName'
	$aMethod[1][0] = 'listPhoneByName'
	$aMethod[2][0] = 'getPhoneTemplate'
	$aMethod[3][0] = 'getPhone'
	$aMethod[4][0] = 'addPhone'
	$aMethod[5][0] = 'removePhone*****'
	$aMethod[6][0] = 'getLine' ;UUID
	$aMethod[7][0] = 'addRoutePattern' ;Fix
	$aMethod[8][0] = 'executeSQLQuery'
	$aMethod[9][0] = 'addLine'
	$aMethod[11][0] = 'updateRoutePattern'

	;10.x
	$aMethod[0][1] = 'listDevicePool'
	$aMethod[1][1] = 'listPhone'
	$aMethod[2][1] = 'getPhoneTemplate'
	$aMethod[3][1] = 'getPhone'  ;all
	$aMethod[4][1] = 'addPhone'
	$aMethod[5][1] = 'removePhone'
	$aMethod[6][1] = 'listline'
	$aMethod[7][1] = 'addRoutePattern'
	$aMethod[8][1] = 'executeSQLQuery'
	$aMethod[9][1] = 'addLine'
	$aMethod[10][1] = 'SelectCmDevice'
	$aMethod[11][1] = 'updateRoutePattern'
#ce

Global	$lineType[1][4] = [['line','pattern', 'description', 'routePartition']] ; The three line types and their necessary fields.
Global	$speeddialType[1][4] = [['speeddial','dirn', 'label', 'asciiLabel']] ; The three line types and their necessary fields.
Global	$busylampType[1][4] = [['busyLampField','blfDirn', 'routePartition', 'label']] ; The three line types and their necessary fields.

Global	Const $aTheHeader[20] = ["name","description","product","class","protocol","protocolSide", _ ;6 elements
								"callingSearchSpaceName", "devicePoolName","commonDeviceConfigName","commonPhoneConfigName","locationName", "mediaResourceListName", _ ;6 elements
								"securityProfileName","useDevicePoolCgpnTransformCss", "userLocale", "networkLocale", "softkeyTemplateName", "networkHoldMohAudioSourceId", _ ; 6 elements
								"userHoldMohAudioSourceId", "status"] 	; 2 elements == 20
								; Need to hardcode the lookup of "line", speeddial", and "busyLampField". We do this because there can be too many to account for.
								; So we build the list dynamically during the time of search



#cs - All Arrays used
	$aXML
	$aXML[0] = $method
	$aXML[1] = $ver
	$aXML[2] = $auth
	$aXML[3] = $host
	$aXML[4] = $postAddress
	$aXML[5] = $SOAP_Action
	$aXML[6] = $strEnvelope

	$aMethod
	$aMethod[0][0] = 'listDevicePoolByName'
	$aMethod[1][0] = 'listPhoneByName'
	$aMethod[2][0] = 'getPhoneTemplate'
	$aMethod[0][1] = 'listDevicePool'
	$aMethod[1][1] = 'listPhone'

	$aText
	$aText[0] = "Status"
	$aText[1] = "Action:"
	$aText[3] = "Item"

	$aParts
	$aParts [0] = 50
	$aParts [1] = 250
	$aParts [2] = 175

	$aTheHeader[20] = ["name","description","product","class","protocol","protocolSide", _ ;6 elements
						"callingSearchSpaceName", "devicePoolName","commonDeviceConfigName","commonPhoneConfigName","locationName", "mediaResourceListName", _ ;6 elements
						"securityProfileName","useDevicePoolCgpnTransformCss", "userLocale", "networkLocale", "softkeyTemplateName", "networkHoldMohAudioSourceId", _ ; 6 elements
						"userHoldMohAudioSourceId", "status"]

	$aH1["AD 10"]
	$aH2["Remove From 10"]



	;Built dynmically

	$7Parsed[$t] - [large]
	$10Parsed[$t] - [large]
	$aDeleteDevices[1]
	$aAddDevices[1]
	$aParsed
	$aHeaders
	$aFields
	$aSaveText1
	$aConfig
	$lineType[1][4]
	$speeddialType[1][4]
	$busylampType[1][4]

#ce

; Initialize COM error handler
$oMyError = ObjEvent("AutoIt.Error","MyErrFunc")

;GUI Variables

;Main GUI
Global	$CUCM_Comparison_GUI, $XML_GUI, $Export_GUI, $iGUIWidth = 572, $iGUIHeight = 591

;Export GUI
Global	$ExportType, $Radio_Add, $Radio_Delete, $Radio_Both, $Group_Export, $Button_Export, _
		$Button_Cancel

;Buttons
Global	$Add_Remove, $Add_Configs, $Build_Phones, $Button_Transform, $Button_Cancel,$Button_Compare_Pool, _
		$Button_Make_XML, $Delete_Phones

;Labels
Global	$Label_Add_Remove, $Label_Get_Configs, $Label_Build_10_New_Phones, $Order_1, $Order_2, $Order_3, _
		$Label_Device_Pool, $Label_Make_XML, $Label_Delete_Old_Phones, $Order_4

;Edit/ListeView
Global	$Add_Edit, $Delete_Edit, $XtoY = 554, $XtoYper = $XtoY/572, $XML_Edit, $Add_Pool, $Delete_Pool

;Tab
Global	$Tab_Window, $Compare_Tab, $Device_Pool_Tab, $Misc_Tab

;Group
Global	$Group_View, $Group_Execute, $Group_Pool, $Group_Make

;Menu/Statusbar
Global	$StatusBar, $File_menu, $Clear_Field, $Separator1, $Exititem, $Export, $Import_List, _
		$aText[3] = ["Status", "Action:", "Item"], _
		$partStatus = 50, _ ;Statusbar
		$partAction= 350/572, _ ;CUCM7
		$partItem = 175/572, _ ;CUCM10
		$aParts[3] = [$partStatus, $partAction * $iGUIWidth, $partItem * $iGUIWidth]


;ComboBox
Global	$ComboDevicePool

;~ Colors
Global	$cText = 0xF1ECDF, $cList = 0xD6D7D9, _
		$MainGUIBKColor = 0x575757; 0x1C1C1F; 0x0121314 ; 0x5C5F58

Global $Theme = _WinAPI_GetThemeAppProperties()

;---------------------------------------------------------------------------------------------

_GUI()

;---------------------------------------------------------------------------------------------

Func _GUI()

	#Region ### START Koda GUI section ### Form=G:\is-voip\djthornton\Scripts\CUCM Scripts\Post To CUCM\AXL SOAP.kxf
	$CUCM_Comparison_GUI = GUICreate("CUCM Comparison", $iGUIWidth, $iGUIHeight, -1, -1, $WS_OVERLAPPEDWINDOW)
	GUISetOnEvent($GUI_EVENT_CLOSE, '_Exit', $CUCM_Comparison_GUI)
;~ 	GUISetBkColor($MainGUIBKColor, $CUCM_Comparison_GUI)

	;FileMenu
	$File_menu = GUICtrlCreateMenu("&File")
	$Export = GUICtrlCreateMenuItem("Export", $File_menu)
	GUICtrlSetOnEvent(-1, '_Export_Selection')
	$Clear_Field = GUICtrlCreateMenuItem("Clear Fields", $File_menu)
	GUICtrlSetOnEvent(-1, '_Clear_Jump')
	$Import_List = GUICtrlCreateMenuItem("Import List", $File_menu)
	GUICtrlSetOnEvent(-1, '_Import_List')
	$Separator1 = GUICtrlCreateMenuItem("", $File_menu, 3)
	$Exititem = GUICtrlCreateMenuItem("Exit", $File_menu)
	GUICtrlSetOnEvent(-1, '_Exit')
	GUICtrlSetState($File_menu, @SW_SHOW)

	;Status
	$StatusBar = _GUICtrlStatusBar_Create($CUCM_Comparison_GUI, $aParts, $aText)

	;Tabview
;~ 	_WinAPI_SetThemeAppProperties($STAP_ALLOW_NONCLIENT)
	$Tab_Window = GUICtrlCreateTab(8, 10, 473, 161)
	GUICtrlSetResizing(-1, $GUI_DOCKALL)
;~ 	GUICtrlSetOnEvent(-1 , '_GetTab')

;~ 	_WinAPI_SetThemeAppProperties(0)
	$Compare_Tab = GUICtrlCreateTabItem("CUCM Compare")
	GUICtrlSetResizing(-1, $GUI_DOCKALL)
;~ 	GUICtrlSetColor(-1, $cText)

	;ListView
;~ 	$Add_Edit = _GUICtrlListView_Create($CUCM_Comparison_GUI, "", 9, 181, $XtoY, 172)
	$Add_Edit = GUICtrlCreateListView ("Report", 9, 181, $XtoY, 172)
;~ 	$Delete_Edit = _GUICtrlListView_Create($CUCM_Comparison_GUI, "", 9, 369, $XtoY, 167)
	$Delete_Edit = GUICtrlCreateListView ("Report", 9, 369, $XtoY, 167)

	;Group 1
	$Group_View = GUICtrlCreateGroup("View Only", 31, 39, 201, 121)
	GUICtrlSetFont(-1, 8, 400, 0, "Arial")
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Add_Remove = GUICtrlCreateButton("List Add Remove", 55, 63, 95, 25)
	GUICtrlSetOnEvent($Add_Remove, "_Add_Remove")
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Label_Add_Remove = GUICtrlCreateLabel("List Phones for add/remove", 55, 90, 135, 17)
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Order_1 = GUICtrlCreateLabel("1", 39, 68, 10, 17)
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Order_2 = GUICtrlCreateLabel("2", 39, 118, 10, 17)
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Add_Configs = GUICtrlCreateButton("Add Configs", 55, 111, 75, 25)
	GUICtrlSetOnEvent( -1, '_Add_Configs')
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Label_Get_Configs = GUICtrlCreateLabel("Get configurations for new phones", 55, 139, 166, 17)
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	;Group 2
	GUICtrlCreateGroup("", -99, -99, 1, 1)
	$Group_Execute = GUICtrlCreateGroup("Modify", 257, 39, 201, 121)
	GUICtrlSetFont(-1, 8, 400, 0, "Arial")
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Label_Build_10_New_Phones = GUICtrlCreateLabel("Build new phones", 270, 90, 135, 17)
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

;~ 	$Order_3 = GUICtrlCreateLabel("3", 265, 68, 10, 17)
	$Build_Phones = GUICtrlCreateButton("Build in 10", 270, 63, 75, 25)
	GUICtrlSetOnEvent(-1, '_Build_10')
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$ComboDevicePool = GUICtrlCreateCombo("All Device Pools", 355, 63, 100, 25, BitOR($CBS_DROPDOWNLIST,$CBS_AUTOHSCROLL, $WS_VSCROLL))
	GUICtrlSetOnEvent(-1, '_DevicePool')
	GUICtrlSetResizing(-1, $GUI_DOCKALL)


	$Label_Delete_Old_Phones = GUICtrlCreateLabel("Delete old phones", 270, 139, 135, 17)
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

;~ 	$Order_4 = GUICtrlCreateLabel("4", 265, 118, 10, 17)
	$Delete_Phones = GUICtrlCreateButton("Delete from 10", 270, 111, 75, 25)
	GUICtrlSetOnEvent(-1, '_Delete')
	GUICtrlSetResizing(-1, $GUI_DOCKALL)
	GUICtrlCreateGroup("", -99, -99, 1, 1)

	;Tab 2
	$Misc_Tab = GUICtrlCreateTabItem("Misc")
	$Group_Make = GUICtrlCreateGroup("Make", 31, 39, 201, 121)
	GUICtrlSetFont(-1, 8, 400, 0, "Arial")
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Button_Make_XML = GUICtrlCreateButton("Make XML", 55, 63, 75, 25)
	GUICtrlSetOnEvent($Button_Make_XML, "_MakeCUCMXMLforAU3")
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Label_Make_XML = GUICtrlCreateLabel("Make XML Syntax", 56, 90, 135, 17)
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

;~ 	DllCall("user32.dll", "int", "SetParent", "hwnd", WinGetHandle($Misc_Tab), "hwnd", WinGetHandle($CUCM_Comparison_GUI))
	$XML_Edit  = GUICtrlCreateEdit("Put XML Here...", 8, 182, 555, 355)

	;Tab 3
	$Device_Pool_Tab = GUICtrlCreateTabItem("Device Pools")
	$Group_Pool = GUICtrlCreateGroup("Copmpare", 31, 39, 201, 121)
	GUICtrlSetFont(-1, 8, 400, 0, "Arial")
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Button_Compare_Pool = GUICtrlCreateButton("Compare", 55, 63, 75, 25)
	GUICtrlSetOnEvent($Button_Compare_Pool, "_Add_Remove_Device_Pool")
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Label_Device_Pool = GUICtrlCreateLabel("Compare Device Pools", 56, 90, 135, 17)
	GUICtrlSetResizing(-1, $GUI_DOCKALL)

	$Add_Pool = GUICtrlCreateListView ("Report", 9, 181, $XtoY, 172)
;~ 	GUICtrlSetResizing(-1, $GUI_DOCKAUTO)

	$Delete_Pool = GUICtrlCreateListView ("Report", 9, 369, $XtoY, 167)
;~ 	GUICtrlSetResizing(-1, $GUI_DOCKAUTO)

	GUICtrlCreateTabItem("")
	GUIRegisterMsg($WM_SIZE, "WM_SIZE")
;~ 	_WinAPI_SetThemeAppProperties($Theme)
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###
;~ 	_Test_Fill_List() ;Used for testing
;~ 	_Add_Remove()
;~ 	_Add_Configs()
	While 1
		Sleep(25)
	WEnd


EndFunc

; Resize the status bar when GUI size changes
Func WM_SIZE($hWnd, $iMsg, $wParam, $lParam)
	#forceref $hWnd, $iMsg, $wParam, $lParam
	_GUICtrlStatusBar_Resize($Statusbar)

;~ 	If $boot = False Then _ColWidth()
	Return $GUI_RUNDEFMSG
	_StatusResize()
EndFunc   ;==>WM_SIZE

Func _StatusResize()
	Local	$aGetGUIWidth
	$aGetGUIWidth = WinGetPos('CUCM Migrator')
	$iGUIWidth = $aGetGUIWidth[2]
	$aParts[1] = ($partAction * $iGUIWidth)
	$aParts[2] = ($partItem* $iGUIWidth)
	_GUICtrlStatusBar_SetParts($statusbar, $aParts)



EndFunc

Func _GetTab()
	Local	$active
	$active = GUICtrlRead($Tab_Window, 1)
	MsgBox(64,'Tab',$active)
	MsgBox(64,'Tab',$Device_Pool_Tab)
EndFunc

Func _Add_Remove()
	;Test
	Dim $aH1[1] = ["Add To 10"]
	Dim $aH2[1] = ["Remove From 10"]

	ProgressOn("Compare", "Extracting Data from CUCM 7 and 10.", _
				"This could take several minutes."&@CRLF&"Please wait...", Default, Default, 16)

	_Log("Scanning CUCM 7 & 10")

	_Clear_Fields(True, True, True)

	_Build_Headers($Add_Edit, $aH1, 'Devices')
	_Build_Headers($Delete_Edit, $aH2, 'Devices')
	ProgressSet(10)

	_Build_Add_Delete()
	$ADD_Dimensions = 1

	_Log("Comparison Complete")
	ProgressOff()


EndFunc

Func _Add_Remove_Device_Pool()
	;Test
	Dim $aH1[1] = ["Add To 10"]
	Dim $aH2[1] = ["Remove From 10"]

	ProgressOn("Compare", "Extracting Data from CUCM 7 and 10.", _
				"This could take several minutes."&@CRLF&"Please wait...", Default, Default, 16)

	_Log("Scanning CUCM 7 & 10")

	_Clear_Fields(True, True, True)

	_Build_Headers($Add_Pool, $aH1, 'Device Pools')
	_Build_Headers($Delete_Pool, $aH2, 'Device Pools')
	ProgressSet(10)

	_Compare_Device_Pool()

	_Log("Comparison Complete")
	ProgressOff()


EndFunc

Func _Build_Add_Delete()
	Local	$aDeleteDevices[1], $aAddDevices[1], $I = 1, $Id = '', $return, $ver, $aXML, _
			$7Parsed, $10Parsed, $xy, $yx, $name, $PTN

	$ver = "7"
	_Log("Begining 7 query")

	ProgressSet(26)
;~ 	$aXML = _XML_Formatter($ver, $aMethod[1][0], 'SEP5C5015A992D5')
;~ $version, $method, $value, $value2 = '', $method_Specific = '', $Desc = '')

	$return = _Call($ver, $aMethod[1][0], 'SEP%')

	$7Parsed = _Find($return, 'name')
	_Log("Finished 7 query")


	$ver = "10"
	_Log("Beginning 10 query")
	ProgressSet(50)
;~ 	$aXML = _XML_Formatter($ver, $aMethod[1][1], 'SEP5C5015A992D5')
	$return = _Call($ver, $aMethod[1][1], 'SEP%')
	$10Parsed = _Find($return, 'name')

	_log("Finished 10 query")

	$aXML = ''
	$return = ''

	For $t = 0 To UBound($7Parsed) -  1

		;Take 7 & Check against 10
		$Id = _ArrayBinarySearch($10Parsed, $7Parsed[$t])
		If $Id = -1 Then
			ReDim $aAddDevices[$I]
			$aAddDevices[$I - 1] = $7Parsed[$t]
			_Log("Building Add List: "&$aAddDevices[$I - 1])
			$I += 1

		Else
			;skip (found)
		EndIf

		$Id = ''

	Next

	$I = 1
	ProgressSet(75)

	For $t = 0 To UBound($10Parsed) -  1

		;Take 10 & Check against 7
		$Id = _ArrayBinarySearch($7Parsed, $10Parsed[$t])
		If $Id = -1 Then
			ReDim $aDeleteDevices[$I]
			$aDeleteDevices[$I - 1] = $10Parsed[$t]
			_Log("Building Delete List: "&$aDeleteDevices[$I - 1])
			$I += 1
		Else
			;skip (found)
		EndIf

		$Id = ''

	Next


	$I = ''
	ProgressSet(100)

	_Build_Fields($Add_Edit, $aAddDevices, 'List Device Names')
	_Build_Fields($Delete_Edit, $aDeleteDevices, 'List Device Names')

	$return = ''
	$7Parsed = ''
	$10Parsed = ''
	$aAddDevices = ''
	$aDeleteDevices = ''
	Return

;~ 	_ArrayDisplay($aDeleteDevices, 'Delete')
;~ 	_ArrayDisplay($aAddDevices, 'Add')

EndFunc

;Add configurations to already discovered devices
Func _Add_Configs()
	Local	$iNdexHeader, $iNdexItem, $headerText, $aDeviceListForBuild, $aDeviceInfo, $lCount

	$ADD_Dimensions = 2

	ProgressOn("Extracting", "Extracting configurations from CUCM 7.", _
				"This could take several minutes."&@CRLF&"Please wait...", Default, Default, 16)

	_Log("Retrieving Phone Configs")

	$iNdexHeader = _GUICtrlListView_GetColumnCount($Add_Edit)
	$iNdexItem = _GUICtrlListView_GetItemCount($Add_Edit)
	$headerText = _GUICtrlListView_GetColumn($Add_Edit, 0)

	If Not IsArray($headerText) Or $iNdexHeader = '' Then
		MsgBox(16,'Error', 'You must first add devices in step 1')
		Return
	ElseIf $headerText[5] = 'Add To 10' Then
;~ 		MsgBox(64,'In','Devices Seen:'&@CRLF& 'Header Text: '&$headerText[5]&@CRLF&'Header Index:'&$iNdexHeader& _
;~ 						@CRLF&'Item Index: '&$iNdexItem)

		;Set Array size
		Local $aDeviceListForBuild[$iNdexItem]

		For $b = 0 To $iNdexItem - 1
			$aDeviceListForBuild[$b] = _GUICtrlListView_GetItemText($Add_Edit, $b)
		Next

;~ 		_ArrayDisplay($aDeviceListForBuild, 'Build Array')

	EndIf

	;Clear Add only
	_Log("Clear Fields")

	_Clear_Fields(True, False, True)

	_Log("Build Headers")

	_Build_Headers($Add_Edit, $aTheHeader)


	$lCount =  UBound($aDeviceListForBuild)
	ProgressSet(1)

	For $l = 0 To $lCount - 1

		_Log("Processing: "& $l &' of '& $lCount & ' : ' & Round((($l/($lCount - 1)) * 100), 0) &'%')

		ProgressSet(($l/($lCount - 1)) * 100)

		$aDeviceInfo = _Request_Phone_Configs($aTheHeader, $aDeviceListForBuild[$l])
;~ 		_ArrayDisplay($aDeviceInfo, 'DeviceInfo - In Add_Configs')
		_Build_Fields($Add_Edit, $aDeviceInfo)
;~ 		ExitLoop ; Testing
	Next

	ProgressSet(100)
	_Log("Retrieval of Configs Complete")

	Sleep(2000)
	ProgressOff()

	$aDeviceListForBuild = ''
	$aDeviceInfo = ''

	_MakeArray(False)

	_Build_DevPool_Combo($aTempListView)

EndFunc

;Build phone configurations based on individual line items in the listview.
Func _Request_Phone_Configs($aConfig, $Device)
	Local	$aDeviceInfo[UBound($aConfig)], $aXML, $return, $rInfo, $arInfo, $intEmpty = 0, $lastColumn, $aNewInserts, $pullStatus

	_Log("Querying for Phone Configs")

	$return = _Call('7', $aMethod[3][0], $Device) ;getPhone method
;~ 	MsgBox(64,'Request Return', $return)

	;Std Configs
	For $y = 1 To UBound($aConfig) - 2
		$aDeviceInfo[0] = $Device ; Sometimes another entry w/ "name" is returned. So set it hard here
		$rInfo = _Find($return, $aConfig[$y])
		If Not IsArray($rInfo) Then
			$intEmpty += 1
			$aDeviceInfo[$y] = '' ; Blank out empty fields
		Else
			$aDeviceInfo[$y] = $rInfo[0] ; Set deviceInfo for paramater
		EndIf
	Next

;~ 	_ArrayDisplay($aDeviceInfo, 'Pre-Concatinate')

	$aNewInserts = _Get_Lines_SpeedDials_BusyLamps_fields($return, $Device) ; Get the lines, speeds, and busylamps


;~ 	If $Device = 'SEP28C7CED6D403' Then  _ArrayDisplay($aNewInserts, 'aNewInserts')
	$pullStatus = _ArrayPop($aDeviceInfo)
	_ArrayConcatenate($aDeviceInfo, $aNewInserts)
;~ 	If $Device = 'SEP28C7CED6D403' Then  _ArrayDisplay($aDeviceInfo, 'Post-Concatinate')
	_ArrayAdd($aDeviceInfo, $pullStatus)
;~ 	If $Device = 'SEP28C7CED6D403' Then  _ArrayDisplay($aDeviceInfo, 'aDeviceInfo - After Add')

;~ 	ReDim $aDeviceInfo[UBound($aDeviceInfo) + UBound($aNewInserts)]

;~ 	If $intEmpty <> 0 Then $aDeviceInfo[UBound($aDeviceInfo) - 1] = 'There is/are '&$intEmpty&' empty field(s).'
	If $intEmpty <> 0 Then
			$aDeviceInfo[UBound($aDeviceInfo) - 1] = 'There is/are '&$intEmpty&' empty field(s).'
	Else
		$aDeviceInfo[UBound($aDeviceInfo) - 1] = 'All phone configs are populated'
	EndIf
;~ 	If $Device = 'SEP28C7CED6D403' Then  _ArrayDisplay($aDeviceInfo, 'aDeviceInfo - After Status')

	$aConfig = ''
	$aXML = ''
	$rInfo = ''
	$return = ''
;~ 	$ver = ''

	Return $aDeviceInfo

EndFunc

;Get line, speeddial, and busy lamps sequence and add to list view
Func _Get_Lines_SpeedDials_BusyLamps_fields($return, $Device)
	Local	$alineInfo, $arInfo, $aTemp[1], $intMarker, $aNew[1], $aSpecialInsert, $iNdexofInserts, $searchSpace
	_Log("Device: "&$Device)
	;$searchSpace narrows the search so that there are no returns from other sections. ex: label can return a speed dial label when you want a busy lamp label...
	ReDim $aNew[UBound($aNew) + $Highest_Line_Count]
	$searchSpace = _StringBetween($return, '<lines>', '</lines>')
	If Not @error Then
		$searchSpace = _ArrayToString($searchSpace)
		;Lines if exist. Returns UUIDs
		$arInfo = _Find($searchSpace, 'line')
	;~ 	_ArrayDisplay($arInfo)

		If IsArray($arInfo) Then
			_Log("ProcessingLines")
	;~ 		_ArrayDisplay($arInfo)
	;~ 		MsgBox(64,'aNew 1', UBound($aNew))
			$aTemp = _ArrayExtract($arInfo, 0, UBound($arInfo) -1, 1, 1); Removes column 1 (has order number)

			;Convert UUIDs into usuable info
			$aTemp = _Change_UUID('line', $aTemp)
	;~ 		_ArrayDisplay($aTemp, 'After - before headers')

			;Inject headers into listview
			_InsertHeaders($aTemp, $Device, 'line')

			;Determine how many inserts are needed
			$aSpecialInsert = _ArrayExtract($aTemp, 1, 0, 1, 0)
			$iNdexofInserts = UBound($aSpecialInsert) * UBound($aSpecialInsert, 2)
	;~ 		_ArrayDisplay($aNew , 'line - before')
			$intMarker = 0

			ReDim $aNew[$Highest_Line_Count]

			For $ig = 0 To UBound($aSpecialInsert) - 1
				For $ih = 0 To UBound($aSpecialInsert, 2) - 1
					$aNew[$intMarker] = $aSpecialInsert[$ig][$ih]
					$aNew[$intMarker] = $aSpecialInsert[$ig][$ih]
					$aNew[$intMarker] = $aSpecialInsert[$ig][$ih]
					$intMarker += 1
				Next
			Next

;~ 			_ArrayDisplay($aNew, 'aNew')

			$arInfo = ''
			$aTemp = ''
			$iNdexofInserts = ''
			$aSpecialInsert = ''
			$searchSpace = ''
	;~ 		$intMarker = 0 ; need to keep count for next add
		EndIf

	EndIf

	If Not $Skip Then
;~ ===============================================================================
	;Speeddials if exist
	;$searchSpace narrows the search so that there are no returns from other sections. ex: label can return a speed dial label when you want a busy lamp label...
	ReDim $aNew[UBound($aNew) + $Highest_Speedial_Count] ; Account for size whether this one has speeds or not
;~ 	_ArrayDisplay($aNew, 'aNew')
	$searchSpace = _StringBetween($return, '<speeddials>', '</speeddials>')
	If Not @error Then
		$searchSpace = _ArrayToString($searchSpace)
		;Speed dials if exist
		$arInfo = _Find($searchSpace, 'speeddial')

		If IsArray($arInfo) Then
			_Log("Processing Speed Dials")

			_InsertHeaders($arInfo, $Device, 'speeddials')
			ReDim $aNew[UBound($aNew) + $Highest_Speedial_Count]
			;Determine how many inserts are needed
			$aSpecialInsert = _ArrayExtract($arInfo, 1, 0, 1, 0)
			$iNdexofInserts = UBound($aSpecialInsert) * UBound($aSpecialInsert, 2)



			$intMarker = $Highest_Line_Count

			;flaten list to be added to listview
			For $ig = 0 To UBound($aSpecialInsert) - 1
				For $ih = 0 To UBound($aSpecialInsert, 2) - 1
					$aNew[$intMarker] = $aSpecialInsert[$ig][$ih]
					$aNew[$intMarker] = $aSpecialInsert[$ig][$ih]
					$aNew[$intMarker] = $aSpecialInsert[$ig][$ih]
					$intMarker += 1
				Next
			Next
	;~ 		_ArrayDisplay($aNew, 'aNew after build on speed')

			$arInfo = ''
			$aTemp = ''
			$iNdexofInserts = ''
			$aSpecialInsert = ''
			$searchSpace = ''
		EndIf
	EndIf
;~ ===============================================================================
	ReDim $aNew[UBound($aNew) + $Highest_BusyLamp_Count] ; Account for size whether this one has busy or not or not
	$searchSpace = _StringBetween($return, '<busyLampFields>', '</busyLampFields>')
	If Not @error Then
;~ 		_ArrayDisplay($searchSpace, 'searchspace')
		$searchSpace = _ArrayToString($searchSpace)
;~ 		If $Device = 'SEP28C7CED6D403' Then  MsgBox(64, 'searchspace', $searchSpace)
		;Busylamp if exist
		$arInfo = _Find($searchSpace, 'busyLampField')
;~ 		If $Device = 'SEP28C7CED6D403' Then  _ArrayDisplay($arInfo, 'arInfo')
		If IsArray($arInfo) Then
			_Log("Processing Busy Lamps Fields")
	;~ 		$aTemp = _ArrayExtract($arInfo, 0, UBound($arInfo) -1, , 1); Removes column 1 (has order number)
;~ 			_ArrayDisplay($arInfo, 'aTemp busy - before headers')
			;Inject headers into listview
			_InsertHeaders($arInfo, $Device, 'busyLampField')
			ReDim $aNew[UBound($aNew) + $Highest_BusyLamp_Count]
			;Determine how many inserts are needed
			$aSpecialInsert = _ArrayExtract($arInfo, 1, 0, 1, 0)
			$iNdexofInserts = UBound($aSpecialInsert) * UBound($aSpecialInsert, 2)
;~ 			If $Device = 'SEP28C7CED6D403' Then  MsgBox(64,'Highest BusyLamp', $Highest_BusyLamp_Count)
;~ 			If $Device = 'SEP28C7CED6D403' Then  _ArrayDisplay($aNew, "After Redim")
;~ 			MsgBox(64,'IntMarker - busyLampField', $intMarker)
			$intMarker = $Highest_Line_Count + $Highest_Speedial_Count

			;flaten list to be added to listview
			For $ig = 0 To UBound($aSpecialInsert) - 1
				For $ih = 0 To UBound($aSpecialInsert, 2) - 1
					$aNew[$intMarker] = $aSpecialInsert[$ig][$ih]
					$aNew[$intMarker] = $aSpecialInsert[$ig][$ih]
					$aNew[$intMarker] = $aSpecialInsert[$ig][$ih]
					$intMarker += 1
				Next
			Next
;~ 			If $Device = 'SEP28C7CED6D403' Then  _ArrayDisplay($aNew, 'aNew after build on busy')

			$arInfo = ''
			$aTemp = ''
			$iNdexofInserts = ''
			$aSpecialInsert = ''
			$searchSpace = ''
		EndIf
	EndIf
	EndIf
	$intMarker = 0
	$return = ''
	Return $aNew

;~ ===============================================================================
	;Busylamps if exist - 'busyLampField'

EndFunc

Func _Build_10()
	Local	$aDeviceInfo, $aXML, $ver = 10, $return, $aParsed, $iNdexColumn, $iNdexRows, $eNvelope, _
			$aFault, $auuid, $xString, $logDir = @ScriptDir&'\Logs', $devName

	_Build_Save()
;~ Return ; While building
;~ 	MsgBox(64,'Check Export','Check that the auto export worked.')

	$action = "Building Devices"
	ConsoleWrite($action&@CRLF)
	_GUICtrlStatusBar_SetText($StatusBar, 'Action: ' & $action, 1)
	ProgressOn("Building", "Building phones into CUCM 10.", _
				"This could take several minutes."&@CRLF&"Please wait...", Default, Default, 16)

	$iNdexColumn = UBound($aTheHeader)
	$iNdexRows = _GUICtrlListView_GetItemCount($Add_Edit)
;~ 	MsgBox(64,'Column Index', $iNdexColumn)
;~ 	MsgBox(64,'Row Index', $iNdexRows)

	DirCreate($logDir)
	For $r = 0 To $iNdexRows - 1
		$devName = _GUICtrlListView_GetItemText($Add_Edit, $r, 0)
		$aXML = _XML_Formatter($ver, $aMethod[4][1], $devName)

		_Log("Processing: "& $r+1 &' of '& $iNdexRows & ' : ' & Round((($r/($iNdexRows-1)) * 100), 0) &'%')

		ProgressSet(($r/($iNdexRows - 1)) * 100)

		For $c = 1 To UBound($aTheHeader) - 1 ; Represents all the phone basics. Starting from 1 because we've already got the name

;~ 			MsgBox(64,'Compare', $aXML[5]& @CRLF&@CRLF&'<'&$aTheHeader[$c]&'>?</'&$aTheHeader[$c]&'>'& @CRLF&@CRLF&_GUICtrlListView_GetItemText($Add_Edit, $r, $c))


			$aXML[5] = StringReplace($aXML[5], '<'&$aTheHeader[$c]&'>?</'&$aTheHeader[$c]&'>', _ ; For the rest
									'<'&$aTheHeader[$c]&'>'& _
									_GUICtrlListView_GetItemText($Add_Edit, $r, $c)& _
									'</'&$aTheHeader[$c]&'>')

		Next

;~ 		MsgBox(64,'$aXML[5]', $aXML[5])

		$return = _SOAP_Open($aXML[0], $aXML[1], $aXML[2], $aXML[3], $aXML[4], $aXML[5])

;~ 		MsgBox(64,'Return', $return)

		$aFault = _Find($return, 'faultstring')
		$auuid = _Find($return, 'return')

		If IsArray($aFault) Then
			_GUICtrlListView_AddSubItem($Add_Edit, $r, 'Failed - '&$aFault[0], $iNdexColumn - 1)
			_FileWriteLog($logDir&'\10 Phone Build.csv', 'Failed,'&$devName&','&$aFault[0])
		ElseIf IsArray($auuid) Then
			_GUICtrlListView_AddSubItem($Add_Edit, $r,'Successful - '&$auuid[0], $iNdexColumn - 1)
			_FileWriteLog($logDir&'\10 Phone Build.csv', 'Successful,'&$devName&','&$auuid[0])
		Else
			_GUICtrlListView_AddSubItem($Add_Edit, $r, 'Failed - '&$return, $iNdexColumn - 1)
			_FileWriteLog($logDir&'\10 Phone Build.csv', 'Failed,'&$devName&','&$return)
		EndIf

;~ 		MsgBox(64,'Check Log', $logDir)

	Next

	ProgressOff()

EndFunc

Func _Delete()
	Local	$aSaveText1, $iColumnNdex_delete, $iRowNdex_Delete, $colHeader, $exp_Return, _
			$Backups = @ScriptDir&'\Backups', $aXML, $return, $pgbar = 0, $Dtotal, _
			$timeStamp = '['&@YEAR&"-"&@MON&"-"&@MDAY&" "&@HOUR&"-"&@MIN&"-"&@SEC&"]", _
			$log = $Backups&'\Deleted Phones.log'

	_Log("Saving List")
	$exp_Return = _Export_Jump()
	_Log("Save Complete")

	If $exp_Return <> 0 Then Return

	$iRowNdex_Delete = _GUICtrlListView_GetItemCount($Delete_Edit)

	Dim $aSaveText1[$iRowNdex_Delete]


	For $k = 0 To $iRowNdex_Delete - 1

		$aSaveText1[$k] = _GUICtrlListView_GetItemText($Delete_Edit, $k)

	Next

	If Not IsArray($aSaveText1) Or $aSaveText1[0] = '0' Then
		MsgBox(16, "Canceled", "There are no devices to remove.")
		Return
	EndIf

;~ 	_ArrayDelete($aSaveText1, 0)
;~ 	_ArrayDisplay($aSaveText1)

	;This is where we try and make some sort of failsafe in the form of backup info|$aMethod[3][0] = 'getPhone'

	ProgressOn("Backup", "Backingup CUCM 10 Phones.", _
				"This could take several minutes."&@CRLF&"Please wait...", Default, Default, 16)
	$Dtotal = UBound($aSaveText1)

	_Log("Backing up phones and deleting")
	If Not FileExists($Backups) Then DirCreate($Backups)

	For $Device In $aSaveText1

		;Query Config Code
		$return = _Call('10', $aMethod[3][1], $Device) ;getPhone method

		;Save Configs
		FileWrite($Backups&'\'&$Device&'.xml', $return)

		;Check results in directory
		If $pgbar < 3 Then MsgBox(64,'Check Output', $Backups)

		;Deletion Code
		$return = _Call('10', $aMethod[5][1], $Device) ;getPhone method

		;Log
		If StringInStr($return, "soapenv:Fault") Then
			FileWriteLine($log, $timeStamp&','&$Device&',Failed')
		Else
			FileWriteLine($log, $timeStamp&','&$Device&',Success')
		EndIf

		;Increase Progress Bar
		ProgressSet(100*($pgbar/$Dtotal))
		$pgbar += 1
	Next

	ProgressSet(100)
	Sleep(1000)
	ProgressOff()

	_Log("Backup & deletion complete")

EndFunc

Func _Change_UUID($type, $auuid)
	Local	$aXML, $return, $aTemp, $intLines = 1

	_Log("Converting UUIDs")

	Switch	$type

		Case 'line'
			$aTemp = $lineType
;~ 			_ArrayDisplay($aTemp, 'aTemp')
			$intLines = UBound ($aTemp)

			For $y = 0 To UBound($auuid) - 1
				$return = _Call('7', $aMethod[6][0], $auuid[$y], '', 'uuid') ;getLine method
;~ 				MsgBox(64,'line return', $return)
				$aTemp = _Find($return, $aTemp)
;~ 				$lineType[1][4] = [['line','pattern', 'description', 'routePartition']] ; reference
			Next

			Return $aTemp

		Case 'routePartition'

	EndSwitch

EndFunc

;Inject lines, speeddials and busylampfields
Func _InsertHeaders($aSpecialInsert, $Device, $type)
	Local	$aInsertColumnHeaders, $iNdexofInserts, $iNdexofColumns, $intMarker2, $modifier
	;Note that for each row the entire column must be inserted each time. The header will be inserted then its corresponding subitem then the same on the next row.
	$action = "Building Insert Headers"
	ConsoleWrite($action&@CRLF)
	_GUICtrlStatusBar_SetText($StatusBar, 'Action: ' & $action, 1)

	;extract headers
	Dim $aInsertColumnHeaders[3] = [$aSpecialInsert[0][1],$aSpecialInsert[0][2],$aSpecialInsert[0][3]]

	$aSpecialInsert = _ArrayExtract($aSpecialInsert, 1, 0, 1, 0)
	$iNdexofInserts = UBound($aSpecialInsert) * UBound($aSpecialInsert, 2)

	If $ColumnInsertPoint = 0 Then $ColumnInsertPoint = _GUICtrlListView_GetColumnCount($Add_Edit)

	Switch $type
		Case 'line'
			If $Highest_Line_Count > $iNdexofInserts Then
				Return
			Else
				$modifier = $Highest_Line_Count  ; Modifiers purpose is to account for the extra spaces after last largest. We don't want to make them again
				$Highest_Line_Count = $iNdexofInserts
				$Line_Start = $ColumnInsertPoint
				If $1st = True Then
					$intMarker2 = $Line_Start
					$1st = False
				Else
					$intMarker2 = $Line_Start + $modifier
				EndIf
;~ 				MsgBox(64,'Mod', $modifier)
;~ 				MsgBox(64,'Higest After = iNdex', $Highest_Line_Count)
			EndIf

		Case 'speeddials'
			If $Highest_Speedial_Count > $iNdexofInserts Then
				Return
			Else
				$modifier = $Highest_Speedial_Count
				$Highest_Speedial_Count = $iNdexofInserts
				$Speed_Start = $Line_Start + $Highest_Line_Count
				$intMarker2 = $Speed_Start + $modifier
			EndIf

		Case 'busyLampField'
			If $Highest_BusyLamp_Count > $iNdexofInserts Then
				Return
			Else
				$modifier = $Highest_BusyLamp_Count
;~ 				MsgBox(64,'$modifier', $modifier)
				$Highest_BusyLamp_Count = $iNdexofInserts
;~ 				MsgBox(64,'$Highest_BusyLamp_Count', $Highest_BusyLamp_Count)
				$BusyLamp_Start = $Speed_Start + $Highest_Speedial_Count
				If $BusyLamp_Start = 0 Then $BusyLamp_Start = ($Line_Start + $Highest_Line_Count)
;~ 				MsgBox(64,'$BusyLamp_Start', $BusyLamp_Start)
				$intMarker2 = $BusyLamp_Start + $modifier
;~ 				MsgBox(64,'$intMarker2', $intMarker2)
			EndIf
	EndSwitch


	_Log("Inserting Headers")

;~ 	If $Device = 'SEP5C5015A992D5' Then MsgBox(64,'Special', UBound($aSpecialInsert))
;~ 	If $Device = 'SEP5C5015A992D5' Then MsgBox(64,'Column', UBound($aInsertColumnHeaders))
;~ 	If $Device = 'SEP5C5015A992D5' Then MsgBox(64,'Modifier', $modifier)
;~ 	If $Device = 'SEP5C5015A992D5' Then MsgBox(64,'intMarker', $intMarker2)

;~ 	For $ib = 0 + $modifier To UBound($aSpecialInsert) - 1 ; modifier scoots the starting point forward so you don't overwrite the columns already made
;~ 		For $iC = 0 To UBound($aInsertColumnHeaders) - 1
;~ 			MsgBox(64,'Insert Column',_GUICtrlListView_InsertColumn($Add_Edit, $intMarker2 - 1, $aInsertColumnHeaders[$iC]))
;~ 			_GUICtrlListView_InsertColumn($Add_Edit, $intMarker2 - 1, $aInsertColumnHeaders[$iC])
;~ 			$intMarker2 += 1
;~ 		Next
;~ 	Next


	For $ib = 0 + ($modifier/3) To UBound($aSpecialInsert) - 1 ; modifier scoots the starting point forward so you don't overwrite the columns already made
		For $iC = 0 To UBound($aInsertColumnHeaders) - 1
;~ 			MsgBox(64,'Insert Column',_GUICtrlListView_InsertColumn($Add_Edit, $intMarker2 - 1, $aInsertColumnHeaders[$iC]))
			_GUICtrlListView_InsertColumn($Add_Edit, $intMarker2 - 1, $aInsertColumnHeaders[$iC])
			$intMarker2 += 1
		Next
	Next

EndFunc

;Find information in SOAP returns
Func _Find($input, $find)
	Local	$name, $aParsed, $hold = True, $aLines, $aliNdex, $auuIDs, $aPatterns, $pattern_iNdex, _
			$aSpeed, $aBusy1, $aBusy2, $aBusy3, $aspeed1, $aspeed2, $aspeed3, $bigAssArray[3], $intTrack, $aTemp
	;Parse & Sort

	If $find = 'line'  Then

		$action = "Finding Lines"
		ConsoleWrite($action&@CRLF)
		_GUICtrlStatusBar_SetText($StatusBar, 'Action: ' & $action, 1)

		$aLines = _StringBetween($input, "<"&$find&' index="','"')

		If Not IsArray($aLines) Then Return 'No Lines'

;~ 		_ArrayDisplay($alines, 'aLines')

		$auuIDs = _StringBetween($input, '<dirn uuid="{','}"/>')

;~ 		_ArrayDisplay($auuIDs, 'auuIDs')

		$pattern_iNdex = UBound($aLines)

		Dim $aPatterns[$pattern_iNdex][2]

		For $h = 0 To $pattern_iNdex - 1

			$aPatterns[$h][0] = $alines[$h]
			$aPatterns[$h][1] = $auuIDs[$h]

		Next

		$aLines = ''
		$auuIDs = ''
		$aliNdex = ''
		_ArraySort($aPatterns)
		Return  $aPatterns

	ElseIf $find = 'speeddial' Then ;Speeddials

		$action = "Finding Speed dials"
		ConsoleWrite($action&@CRLF)
		_GUICtrlStatusBar_SetText($StatusBar, 'Action: ' & $action, 1)
;~ 		MsgBox(64,'Input', $input)
		$aLines = _StringBetween($input, "<"&$find&' index="','">')
;~ 		_ArrayDisplay($aLines)

		If Not IsArray($aLines) Then Return 'No Speed Dials'

		$aspeed1 = _StringBetween($input, '<dirn>','</dirn>')
		$aspeed2 = _StringBetween($input, '<label>','</label>')
		$aspeed3 = _StringBetween($input, '<asciiLabel>','</asciiLabel>')

;~ 		_ArrayDisplay($aspeed1, '1')
;~ 		_ArrayDisplay($aspeed2, '2')
;~ 		_ArrayDisplay($aspeed3, '3')

		$pattern_iNdex = UBound($aLines) + 1 ;adding space for headers
;~ 		MsgBox(64,'Count', $pattern_iNdex)

		$aPatterns = $speeddialType

;~ 		_ArrayDisplay($aPatterns, 'patterns 1')

		ReDim $aPatterns[$pattern_iNdex][4] ;Add one for the headers

;~ 		_ArrayConcatenate($aPatterns, $speeddialType)

;~ 		_ArrayDisplay($aPatterns, 'patterns')

		For $h = 1 To $pattern_iNdex - 1

			$aPatterns[$h][0] = $aLines[$h - 1]
			$aPatterns[$h][1] = $aspeed1[$h - 1]
			$aPatterns[$h][2] = $aspeed2[$h - 1]
			$aPatterns[$h][3] = $aspeed3[$h - 1]
		Next

		$aLines = ''
		$aspeed1 = ''
		$aspeed2 = ''
		$aspeed3 = ''
		$aliNdex = ''
		Return  $aPatterns

	ElseIf $find = 'busyLampField' Then

		$action = "Finding Busy Lamps Fields"
		ConsoleWrite($action&@CRLF)
		_GUICtrlStatusBar_SetText($StatusBar, 'Action: ' & $action, 1)

		$aLines = _StringBetween($input, "<"&$find&' index="','"')

;~ 		_ArrayDisplay($aLines, 'aLines')

		If Not IsArray($aLines) Then
			Return 'No Busy Lamps'
		ElseIf StringInStr($input, 'blfDirn') And StringInStr($input, 'blfDest') Then
			$aBusy1 = _StringBetween($input, '<blfDirn>','</blfDirn>')
			_ArrayConcatenate($aBusy1, _StringBetween($input, '<blfDest>','</blfDest>'))
			$aBusy2 = 'voip-5digit'
			$aBusy3 = _StringBetween($input, '<label>','</label>')
		ElseIf StringInStr($input, 'blfDirn') Then
			$aBusy1 = _StringBetween($input, '<blfDirn>','</blfDirn>')
			$aBusy2 = _StringBetween($input, '<routePartition>','</routePartition>')
			$aBusy3 = _StringBetween($input, '<label>','</label>')
		ElseIf StringInStr($input, 'blfDest') Then ;wrong configuration so we will fix it here
			$aBusy1 = _StringBetween($input, '<blfDest>','</blfDest>')
			$aBusy2 = 'voip-5digit'
			$aBusy3 = _StringBetween($input, '<label>','</label>')
		EndIf

		$pattern_iNdex = UBound($aLines) + 1 ;adding space for headers

;~ 		MsgBox(64,'Pattern_iNdex', $pattern_iNdex)

		$aPatterns = $busylampType


		ReDim $aPatterns[$pattern_iNdex][4]
;~ 		_ArrayDisplay($aPatterns, '1st aPatterns')
;~ 		_ArrayDisplay($aBusy1, '1st Busy')

		For $h = 1 To $pattern_iNdex - 1

			$aPatterns[$h][0] = $aLines[$h - 1]
			If IsArray($aBusy1) Then
				$aPatterns[$h][1] = $aBusy1[$h - 1]
			Else
				$aPatterns[$h][1] = 'Error'
				ContinueLoop
			EndIf

			If IsArray($aBusy2) Then ; This section has been known to be entered incorrectly in CUCM. We need to fix it here.
				$aPatterns[$h][2] = $aBusy2[$h - 1]
			Else
				$aPatterns[$h][2] = 'voip-5digit'
			EndIf

			If IsArray($aBusy3) Then
				If $h > UBound($aBusy3) -1 Then
					$aPatterns[$h][3] = 'Error'
				Else
					$aPatterns[$h][3] = $aBusy3[$h - 1]
				EndIf
			Else
				$aPatterns[$h][3] = ' '
			EndIf

;~ 			_ArrayDisplay($aPatterns, 'Building aPatterns')

		Next

		$aLines = ''
		$aBusy1 = ''
		$aBusy2 = ''
		$aBusy3 = ''
		$aliNdex = ''
		Return  $aPatterns

	ElseIf IsArray($find) Then
		Local	$iDim = UBound($find, 0)

		_Log("Finding Phone Configs")

		If $iDim = 1 Then
			;If 1 dimenstion do something, nothing at the moment though
		ElseIf $iDim = 2 Then

			Switch $find[0][0]
				Case 'line'
;~ 					_ArrayDisplay($find, 'Find')
;~ 					$lineType[1][4] = [['line','pattern', 'description', 'routePartition']] reference
					For $ai = 1 To UBound($find, 2) - 1
						$aParsed = _StringBetween($input, "<"&$find[0][$ai]&">", "</"&$find[0][$ai])
;~ 							_ArrayDisplay($aParsed, 'aParsed')
						If IsArray($aParsed) And $ai = 1 Then
							ReDim $find[UBound($find) + 1][4]
;~ 								_ArrayDisplay($find, "redim'ed find")
						ElseIf $ai = 3 Then
							;get routepartition 'routePartition uuid="{'
							$aParsed = _StringBetween($input, '<'&$find[0][$ai]&' uuid="{', '}"/>')
						EndIf

						If IsArray($aParsed) Then $find[UBound($find) - 1][$ai] = $aParsed[0]
						If IsArray($aParsed) Then
							_Log("Adding: "&$aParsed[0]&" to find array")
						Else
							_Log("Failed adding. Nothing found for "&$find[0][$ai]&".")
						EndIf


					Next
;~ 					_ArrayDisplay($find, 'Find')
;~ 					MsgBox(64,'Find', $find)
					$iDim = ''
					$aParsed = ''
					$input = ''
					Return $find

				Case 'speedial'


				Case 'busyLampField'

			EndSwitch
		EndIf

	Else

		$aParsed = _StringBetween($input, "<"&$find&">", "</"&$find&">")
	;~ 	If @error Then MsgBox(16,'Error', 'Find: '&$find)
		_ArraySort($aParsed)

		Return $aParsed

	EndIf


EndFunc

;Build headers for ListView
Func _Build_Headers($ListView, $aHeaders, $style = '')
	Local	$iHeaders

;~ 	_ArrayDisplay($aHeaders, $ListView)

	_Log("Building Headers")

	$iHeaders = UBound($aHeaders)

	For $a = 0 To $iHeaders - 1

		_GUICtrlListView_AddColumn($ListView, $aHeaders[$a], 200)

	Next

	$aHeaders = ''

	Return

EndFunc

;Build the fields in the ListView
Func _Build_Fields($ListView, $aFields, $style = '', $iNdex = '')
	Local	$iFields, $iHeaders, $i

;~ 	_ArrayDisplay($aFields, 'In _Build_Fields - Fields')
	_Log("Building Fields")

	If $style = 'List Device Names' Then

		$iFields = UBound($aFields)
		For $a = 0 To $iFields - 1
			$i = _GUICtrlListView_AddItem($ListView, $aFields[$a])
		Next

	ElseIf $style = $aMethod[3][0] Then

		$i = _GUICtrlListView_AddItem($ListView, $aFields[0])

		$iFields = UBound($aFields)
		For $a = 1 To $iFields - 1
			_GUICtrlListView_AddSubItem($ListView, $iNdex, $aFields[$a], $a)
		Next

	Else

		$iFields = UBound($aFields)
		$iHeaders = _GUICtrlListView_GetColumnCount($ListView)
		$i = _GUICtrlListView_AddItem($ListView, $aFields[0])
		For $a = 1 To $iFields - 1

			_GUICtrlListView_AddSubItem($ListView, $i, $aFields[$a], $a)

		Next
	EndIf

	$aFields = ''

	Return

EndFunc

;Build AXL for "#include <.\soap_xml.au3>"
Func _MakeCUCMXMLforAU3()
	Local	$read, $write, $aString, $t = 0, $aTemp[1]

	$read = GUICtrlRead($XML_Edit)
	$aString = StringSplit($read, @CR)

;~ 	MsgBox(64, 'String No.', $aString[0])
;~ 	_ArrayDisplay($aString)


	For $a = 1 To UBound($astring) - 1
		If StringInStr($astring[$a] , "<!") Then
			;skip
		Else
			ReDim $aTemp[UBound($aTemp) + 1]
			$aTemp[$t] = StringReplace($aString[$a],"<", "'<", 1)
			If $a = UBound($astring) - 1 Then
				$aTemp[$t] = StringReplace($aTemp[$t],">", ">'", -1)
			Else
				$aTemp[$t] = StringReplace($aTemp[$t],">", ">'&@CR& _", -1)
			EndIf
			$t += 1
		EndIf
	Next

	GUICtrlSetData($XML_Edit, _ArrayToString($aTemp, @CR))


EndFunc

;Device Pool comparison tab
Func _Compare_Device_Pool()

	Local	$aDeleteDevices[1], $aAddDevices[1], $I = 1, $Id = '', $return, $ver, $aXML, _
			$7Parsed, $10Parsed, $xy, $yx, $name, $PTN

	$ver = "7"
	_Log("Begining 7 query")

	ProgressSet(26)
;~ 	$aXML = _XML_Formatter($ver, $aMethod[1][0], 'SEP5C5015A992D5')
	$return = _Call($ver, $aMethod[0][0], '%')
	$7Parsed = _Find($return, 'name')
	_Log("Finished 7 query")


	$ver = "10"
	_Log("Beginning 10 query")
	ProgressSet(50)
;~ 	$aXML = _XML_Formatter($ver, $aMethod[1][1], 'SEP5C5015A992D5')
	$return = _Call($ver, $aMethod[0][1], '%')
	$10Parsed = _Find($return, 'name')

	_log("Finished 10 query")

	$aXML = ''
	$return = ''

	For $t = 0 To UBound($7Parsed) -  1

		;Take 10 & Check against 7
		$Id = _ArrayBinarySearch($10Parsed, $7Parsed[$t])
		If $Id = -1 Then
			ReDim $aAddDevices[$I]
			$aAddDevices[$I - 1] = $7Parsed[$t]
			_Log("Building Add List: "&$aAddDevices[$I - 1])
			$I += 1

		Else
			;skip (found)
		EndIf

		$Id = ''

	Next

	$I = 1
	ProgressSet(75)

	For $t = 0 To UBound($10Parsed) -  1

		;Take 10 & Check against 7
		$Id = _ArrayBinarySearch($7Parsed, $10Parsed[$t])
		If $Id = -1 Then
			ReDim $aDeleteDevices[$I]
			$aDeleteDevices[$I - 1] = $10Parsed[$t]
			_Log("Building Delete List: "&$aDeleteDevices[$I - 1])
			$I += 1
		Else
			;skip (found)
		EndIf

		$Id = ''

	Next


	$I = ''
	ProgressSet(100)

	_Build_Fields($Add_Pool, $aAddDevices, 'List Device Names')
	_Build_Fields($Delete_Pool, $aDeleteDevices, 'List Device Names')

	$return = ''
	$7Parsed = ''
	$10Parsed = ''
	$aAddDevices = ''
	$aDeleteDevices = ''
	Return


EndFunc

;Build phone XML
Func _Populate_XML($aSrc, $aDest)
	Local	$aCombination[5], $aLeftOvers, $sParameter, $aParaValue, $iNdex, $intDest = 1, $iCombo

	;Add headers
	For $xc = 0 To 4
		$aCombination[$xc] = $aDest[$xc]
	Next

	For $xml = 6 To UBound($aSrc) - 1 ; 6 is where the non static data begins
		If StringLeft($aSrc[$xml], 2) <> '<!' Then
			$aParaValue = _Grab_XML($aSrc[$xml]) ; [0] = parameter [1] = value
			If IsArray($aParaValue) Then
				$iNdex = _ArraySearch($aDest, $aParaValue[0], 0, 0, 0, 1)  ; 4 or 5 is where the non static data begins
				If Not @error Then
					ReDim $aCombination[UBound($aCombination) + 1]
					$iCombo = UBound($aCombination) - 1
					$aCombination[$iCombo] = $aDest[$iNdex]
					$aCombination[$iCombo] = StringReplace($aCombination[$iCombo], '?', $aParaValue[1], -1)
				Else
;~ 					MsgBox(64,'Ooops', 'Parameter: '&$aParaValue[0])
				EndIf
			ElseIf StringRight($aParaValue, 1) = '/' Then
				;Skip
			Else
				$iNdex = _ArraySearch($aDest, $aParaValue, 0, 0, 0, 1)  ; 4 or 5 is where the non static data begins
				If Not @error Then
					ReDim $aCombination[UBound($aCombination) + 1]
					$iCombo = UBound($aCombination) - 1
					$aCombination[$iCombo] = $aDest[$iNdex]
				Else
;~ 					MsgBox(64,'Ooops 2', 'Parameter: '&$aParaValue)
				EndIf
			EndIf

		EndIf
	Next

	;Add footer

;~ 	MsgBox(64,'Numbers', "Normal: "&UBound($aDest)&@CRLF&"New: "&UBound($aDest) - $intDest)

	ReDim $aCombination[UBound($aCombination) + 5]

	$iCombo = UBound($aCombination) - 1

	For $xc = $iCombo To $iCombo - 4 Step -1
		$aCombination[$xc] = $aDest[UBound($aDest) - $intDest]
		$intDest += 1
	Next

	Return $aCombination
EndFunc

;Get phone XML data to populate build phone XML above
Func _Grab_XML($lSrc)
	Local	$aParameter, $sParameter, $lValue, $aParaValue[2]

;~ 	MsgBox(64,'Line', $lSrc)


	$aParameter = _StringBetween($lSrc, '<', '>')
	$sParameter = $aParameter[0]
	If @error Then
		MsgBox(16, 'Fatal Error', 'There is a problem with the format of the XML data: '&@CRLF&$lSrc&@CRLF&'Please check all bracket enclosures.')
		Exit
	EndIf


;~ 	MsgBox(64,'Parametr', 'Pattern: '&$sParameter&@CRLF&'Open: '&'<'&$sParameter&'>'&@CRLF&'Close: '&'</'&$sParameter&'>')

	$lValue = _StringBetween($lSrc, '<'&$sParameter&'>', '</'&$sParameter&'>')

	If @error  Or $sParameter = 'releaseCause' Then
		If StringRight($sParameter, 3) = '}"/' Then
			$aParameter = _StringBetween($lSrc, '<', ' uuid="{')
			$sParameter = $aParameter[0]
			$lValue = _StringBetween($lSrc, '<'&$sParameter&' uuid="{', '}"/>')
			Switch $lValue[0]
				Case	'6814CD32-77AD-2F7E-9946-F93B3AF270D7'
					$sParameter = $sParameter&'Name'
					$lValue[0] =  'voip-5digit'
				Case	'DFB8C2BB-4E3F-B0D0-1737-67450F9039CE'
					$sParameter = $sParameter&'Name'
					$lValue[0] =  'VOIP-5Digit'
				Case	'AD243D17-98B4-4118-8FEB-5FF2E1B781AC'
					$sParameter = $sParameter&'Name'
					$lValue[0] =  'Standard Presence group'
				Case	'No Error'
					$sParameter = 'releaseClause'
				Case	Else
					MsgBox(16,'Unknown', 'Parameter: '&$sParameter&@CRLF&'Value: '&$lValue[0])

			EndSwitch

;~ 			MsgBox(64,'@error', $lValue[0])
			$aParaValue[0] = $sParameter
			$aParaValue[1] = $lValue[0]
;~ 			_ArrayDisplay($aParaValue)
			Return $aParaValue
		Else
;~ 			MsgBox(64,'Take a good look', $sParameter)
			Return $sParameter
		EndIf
	Else
		$aParaValue[0] = $sParameter
		$aParaValue[1] = $lValue[0]
;~ 		_ArrayDisplay($aParaValue, 'Grab - aPara')
		Return $aParaValue
	EndIf


EndFunc

Func _Export_Selection()

	#Region ### START Koda GUI section ### Form=g:\is-voip\djthornton\scripts\cucm scripts\post to cucm\export.kxf
	$Export_GUI = GUICreate("Form1", 227, 168, 1111, 430, Default, Default, $CUCM_Comparison_GUI)
	$ExportType = GUICtrlCreateLabel("Which table do you want to export?", 28, 24, 169, 17)
	$Radio_Add = GUICtrlCreateRadio("Add", 24, 80, 49, 17)
	$Radio_Delete = GUICtrlCreateRadio("Delete", 84, 80, 57, 17)
	$Radio_Both = GUICtrlCreateRadio("Both", 168, 80, 41, 17)
	$Group_Export = GUICtrlCreateGroup("Export", 8, 56, 209, 57)
	GUICtrlCreateGroup("", -99, -99, 1, 1)
	$Button_Export = GUICtrlCreateButton("Export", 19, 128, 75, 25)
	GUICtrlSetOnEvent(-1, "_Export")
	$Button_Cancel = GUICtrlCreateButton("Cancel", 136, 128, 75, 25)
	GUICtrlSetOnEvent(-1, '_Cancel')
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

EndFunc

;Don't judge me, this was a last minute add
Func _Build_Save()

	$BuildSave = True
	$BackgroundSave = True

	#Region ### START Koda GUI section ### Form=g:\is-voip\djthornton\scripts\cucm scripts\post to cucm\export.kxf
	$Export_GUI = GUICreate("Form1", 227, 168, 1111, 430, Default, Default, $CUCM_Comparison_GUI)
	$ExportType = GUICtrlCreateLabel("Which table do you want to export?", 28, 24, 169, 17)
	$Radio_Add = GUICtrlCreateRadio("Add", 24, 80, 49, 17)
	GUICtrlSetState($Radio_Add, $GUI_CHECKED )
	$Radio_Delete = GUICtrlCreateRadio("Delete", 84, 80, 57, 17)
	$Radio_Both = GUICtrlCreateRadio("Both", 168, 80, 41, 17)
	$Group_Export = GUICtrlCreateGroup("Export", 8, 56, 209, 57)
	GUICtrlCreateGroup("", -99, -99, 1, 1)
	$Button_Export = GUICtrlCreateButton("Export", 19, 128, 75, 25)
	GUICtrlSetOnEvent(-1, "_Export")
	$Button_Cancel = GUICtrlCreateButton("Cancel", 136, 128, 75, 25)
	GUICtrlSetOnEvent(-1, '_Cancel')
	#EndRegion ### END Koda GUI section ###
	_Export()
	GUICtrlDelete($Export_GUI)

	$BuildSave = False
	$BackgroundSave = False

EndFunc

Func _Export_Jump()
	Global $BackgroundSave, $exp_Return

	$Both = True
	$BackgroundSave = True
	$exp_Return = _Export()
	$Both = False
	$BackgroundSave = False

	Return $exp_Return

EndFunc

Func _Export()

	Local	$sFile, $aSaveText1, $iColumnNdex_add, $iColumnNdex_delete, $iRowNdex_Add, $iRowNdex_Delete, _
			$colHeader

;~ 	GUISetState(@SW_HIDE, $Export_GUI)

	_Log("Exporting Inoformation")

	$iColumnNdex_add = _GUICtrlListView_GetColumnCount($Add_Edit)
	$iRowNdex_Add = _GUICtrlListView_GetItemCount($Add_Edit)

	$iColumnNdex_delete = _GUICtrlListView_GetColumnCount($Delete_Edit)
	$iRowNdex_Delete = _GUICtrlListView_GetItemCount($Delete_Edit)

	If BitAND(GUICtrlRead($Radio_Add), $GUI_CHECKED) = $GUI_CHECKED And $Both = False Then


		_Log("Creating Add File")

;~ 		$aSaveText1 = _GUICtrlListView_GetItemTextArray($Add_Edit)


		Dim $aSaveText1[$iRowNdex_Add + 1][$iColumnNdex_add]

		For $j = 0 To $iColumnNdex_add - 1

			$colHeader = _GUICtrlListView_GetColumn($Add_Edit,$j)
			$aSaveText1[0][$j] = $colHeader[5]

		Next

		For $k = 0 To $iRowNdex_Add - 1

			For $j = 0 To $iColumnNdex_add - 1
				$aSaveText1[$k + 1][$j] = _GUICtrlListView_GetItemText($Add_Edit, $k, $j)
			Next

		Next

;~ 		MsgBox(64,'Ubound', "Dim: "&UBound($aSaveText1, 2)&@CRLF&"Index: "&UBound($aSaveText1))

		If Not IsArray($aSaveText1) Or $aSaveText1[0][0] = '0' Then
			MsgBox(16, "Canceled", "There's no data to save.")
			_Cancel()
			Return "No Save"
		EndIf

;~ 		_ArrayDelete($aSaveText1, 0)
		If $BuildSave = True Then
			$sFile = @ScriptDir&'\Backups\'&GUICtrlRead($ComboDevicePool, 1)
			DirCreate($sFile)
			$sFile = $sFile&'\'&GUICtrlRead($ComboDevicePool, 1)
		Else
			$sFile = FileSaveDialog("Export", "", "Text (csv.*;txt.*)", 18, "CUCM Comparison Export", $CUCM_Comparison_GUI)
		EndIf


		_Write($aSaveText1, "", True, "10 Add", $sFile)

	ElseIf BitAND(GUICtrlRead($Radio_Delete), $GUI_CHECKED) = $GUI_CHECKED And $Both = False Then


		_Log("Creating Delete File")

		Dim $aSaveText1[$iRowNdex_Delete + 1][$iColumnNdex_delete]

		For $j = 0 To $iColumnNdex_delete - 1

			$colHeader = _GUICtrlListView_GetColumn($Delete_Edit,$j)
			$aSaveText1[0][$j] = $colHeader[5]

		Next

		For $k = 0 To $iRowNdex_Delete - 1

			For $j = 0 To $iColumnNdex_delete - 1
				$aSaveText1[$k + 1][$j] = _GUICtrlListView_GetItemText($Delete_Edit, $k, $j)
			Next

		Next

;~ 		MsgBox(64,'Ubound', "Dim: "&UBound($aSaveText1, 2)&@CRLF&"Index: "&UBound($aSaveText1))

		If Not IsArray($aSaveText1) Or $aSaveText1[0][0] = '0' Then
			MsgBox(16, "Canceled", "There's no data to save.")
			_Cancel()
			Return "No Save"
		EndIf

		$sFile = FileSaveDialog("Export", "", "Text (csv.*;txt.*)", 18, "CUCM Comparison Export", $CUCM_Comparison_GUI)

		_Write($aSaveText1, "", True, "10 Delete", $sFile)

	ElseIf BitAND(GUICtrlRead($Radio_Both), $GUI_CHECKED) = $GUI_CHECKED Or $Both = True Then


		_Log("Creating ADD & Delete File")

		Dim $aSaveText1[$iRowNdex_Add + 1][$iColumnNdex_add]

		For $j = 0 To $iColumnNdex_add - 1

			$colHeader = _GUICtrlListView_GetColumn($Add_Edit,$j)
			$aSaveText1[0][$j] = $colHeader[5]

		Next

		For $k = 0 To $iRowNdex_Add - 1

			For $j = 0 To $iColumnNdex_add - 1
				$aSaveText1[$k + 1][$j] = _GUICtrlListView_GetItemText($Add_Edit, $k, $j)
			Next

		Next

;~ 		MsgBox(64,'Ubound', "Dim: "&UBound($aSaveText1, 2)&@CRLF&"Index: "&UBound($aSaveText1))

		If Not IsArray($aSaveText1) Or UBound($aSaveText1, 2) <> $ADD_Dimensions Or $aSaveText1[0][0] = '0' Then
			MsgBox(16, "Canceled", "There's no data in table 1 to save.")
			_Cancel()
			Return "No Save"
		EndIf

		$sFile = FileSaveDialog("Export", "", "Folder (*.*)", 18, "CUCM Comparison Export", $CUCM_Comparison_GUI)

		_Write($aSaveText1, "", True, "10 Add", $sFile)

;~ 		=====================================================================
		Dim $aSaveText1[$iRowNdex_Delete + 1][$iColumnNdex_delete]

		For $j = 0 To $iColumnNdex_delete - 1

			$colHeader = _GUICtrlListView_GetColumn($Delete_Edit,$j)
			$aSaveText1[0][$j] = $colHeader[5]

		Next

		For $k = 0 To $iRowNdex_Delete - 1

			For $j = 0 To $iColumnNdex_delete - 1
				$aSaveText1[$k + 1][$j] = _GUICtrlListView_GetItemText($Delete_Edit, $k, $j)
			Next

		Next

;~ 		MsgBox(64,'Ubound', "Dim: "&UBound($aSaveText1, 2)&@CRLF&"Index: "&UBound($aSaveText1))

		If Not IsArray($aSaveText1)  Or $aSaveText1[0][0] = '0' Then
			MsgBox(16, "Canceled", "There's no data in table 2 to save.")
			_Cancel()
			Return "No Save"
		EndIf

		_Write($aSaveText1, "", True, "10 Delete", $sFile)
	Else

	EndIf

	$aSaveText1 = ''

	If Not $BackgroundSave Then MsgBox(64,'Complete', 'The export process is complete.')

	_Cancel()

EndFunc

Func _Write($output, $ver, $writeOnly = False, $type = '', $location = '')
	Local	$aParsed

	If  $writeOnly = False Then
		#cs
		;Parse & Sort
		$aParsed = _StringBetween($output, "<name>", "</name>")
		_ArraySort($aParsed)

		;Write File
;~ 		_FileWriteFromArray("G:\is-voip\djthornton\Scripts\CUCM Scripts\Post To CUCM\Parsed_Response_"&$ver&".csv", $aParsed)
		Return $aParsed
		#ce
	Else
;~ 		_ArrayDisplay($output)
;~ 		MsgBox(64, "Saving", $location&"_"&$type&".csv")
		_FileWriteFromArray($location&"_"&$type&".csv", $output)

	EndIf

	_Log("Saving: "&$location&"_"&$type&".csv")

EndFunc

Func _Cancel()

	If $Export_GUI Then GUIDelete($Export_GUI)

EndFunc

Func _Clear_Jump()
	_Clear_Fields()
EndFunc

; This is a global array used to keep a realtime array of the listview for sorting by Device pool
Func _MakeArray($Header = True)
	Local	$iRow, $iColumn, $hText

	$iColumn = _GUICtrlListView_GetColumnCount($Add_Edit)

	$iRow = _GUICtrlListView_GetItemCount($Add_Edit)

	If $Header = False Then ;build one
		ReDim	$aTempListView[$iRow + 1][$iColumn]

		For $hl = 0 To $iColumn - 1

			$hText = _GUICtrlListView_GetColumn($Add_Edit, $hl)

			$aTempListView[0][$hl] = $hText[5]

		Next
	Else

		ReDim	$aTempListView[$iRow][$iColumn]

	EndIf

	For $x = 1 To $iRow - 1

		For $y = 0 To $iColumn - 1
			$aTempListView[$x][$y] = _GUICtrlListView_GetItemText($Add_Edit, $x, $y)
		Next

	Next

;~ 	_ArrayDisplay($aTempListView)

EndFunc

Func _Clear_Fields($ADD = True, $DELETE = True, $clean = False)
	Local	$iNdexHeader, $check, $active, $view1, $view2


	$active = GUICtrlRead($Tab_Window, 1)

	If $active = $Compare_Tab Then
		$view1 = $Add_Edit
		$view2 = $Delete_Edit
	ElseIf $active = $Device_Pool_Tab Then
		$view1 = $Add_Pool
		$view2 = $Delete_Pool
	ElseIf $active = $Misc_Tab Then
		GUICtrlSetData($XML_Edit, "")
		Return
	EndIf

	If $ADD Then
		_Log("Clearing Add view")
		$iNdexHeader = _GUICtrlListView_GetColumnCount($view1)
		For $a = 1 To $iNdexHeader
			$check = _GUICtrlListView_DeleteColumn($view1, 0)
		Next
		_GUICtrlListView_DeleteAllItems($view1)
		If $clean = False Then _GUICtrlListView_AddColumn($view1, 'Report')
	EndIf


	If $DELETE Then
		_Log("Clearing Delete view")
		$iNdexHeader = _GUICtrlListView_GetColumnCount($view2)
		For $a = 1 To $iNdexHeader
			_GUICtrlListView_DeleteColumn($view2, 0)
		Next
		_GUICtrlListView_DeleteAllItems($view2)
		If $clean = False Then _GUICtrlListView_AddColumn($view2, 'Report')
	EndIf

	Return

EndFunc

Func _DevicePool()
	Local	$DP_Read, $aDPtemp[1][1], $iColumnText, $dpIndex, $iColIndex, $int = 1

	$DP_Read = GUICtrlRead($ComboDevicePool, 1)

	If $Current_DP = $DP_Read Then Return 'No Change'

	$iColIndex = UBound($aTempListView, 2)

	If $DP_Read = 'All Device Pools' Then
		; Write all items
		$Current_DP = $DP_Read

		$aDPtemp = $aTempListView
	Else

		For $iColumn = 0 To $iColIndex - 1
			$iColumnText = _GUICtrlListView_GetColumn($Add_Edit, $iColumn)
			If $iColumnText[5] = 'devicePoolName' Then
				$dpIndex = $iColumn
				ExitLoop
			EndIf
		Next

		Redim $aDPtemp[1][$iColIndex]


;~ 		MsgBox(64,'dpIndex', $iColumn)

		For $iRow = 0 To UBound($aTempListView) - 1

			If $aTempListView[$iRow][$dpIndex] = $DP_Read Then
				ReDim $aDPtemp[$int][$iColIndex]
				For $iColumn = 0 To $iColIndex - 1
					$aDPtemp[$int - 1][$iColumn] = $aTempListView[$iRow][$iColumn]

				Next
				$int += 1
			EndIf
		Next

		$Current_DP = $DP_Read
	EndIf

	_GUICtrlListView_DeleteAllItems($Add_Edit)

	_GUICtrlListView_AddArray($Add_Edit, $aDPtemp)

	If @error Then
		MsgBox(16, 'Import Error', 'The specified file could not be imported.')
		Return 'Fail'
	EndIf

EndFunc

Func _Import_List()
	Local	$fileLocation, $aImport
	$fileLocation = FileOpenDialog("Import File", '','Text (*.txt;*.csv)', 1)

	_FileReadToArray($fileLocation, $aImport, 0, '|')

	If @error Or Not IsArray($aImport) Then
		MsgBox(16,'Read Error', 'There was a problem reading the file')
		Return
	EndIf

;~ 	_ArrayDisplay($aImport, 'aImport - Raw')

	If Not $aImport[0][0] = 'name' And Not $aImport[0][UBound($aImport) -1] = 'status' Then
		MsgBox(16,'Match Error', 'The file attempted to be imported is not correctly formatted. Please check the file and try again')
		Return 'Error'
	EndIf



	_Build_DevPool_Combo($aImport)

	_Clear_Fields(True, False, True)

	;create columns for import
	For $dp = 0 To UBound($aImport, 2) - 1
		_GUICtrlListView_AddColumn($Add_Edit, $aImport[0][$dp], 100)
	Next

	_ArrayDelete($aImport, 0)

;~ 	_ArrayDisplay($aImport, 'aImport - Post Delete')

	_GUICtrlListView_AddArray($Add_Edit, $aImport)

	If @error Then
		MsgBox(16, 'Import Error', 'The specified file could not be imported.')
		Return 'Fail'
	EndIf

	$aTempListView = $aImport


EndFunc

Func _Build_DevPool_Combo($aImport)
	Local	$aDP[1], $dpIndex

	_GUICtrlComboBox_ResetContent($ComboDevicePool)
	_GUICtrlComboBox_AddString($ComboDevicePool, "All Device Pools")

	;Getp Device Pool subIndex #
	For $dp = 0 To UBound($aImport, 2) - 1
		If $aImport[0][$dp] = 'devicePoolName' Then $dpIndex = $dp
	Next

	;Get All Unique Device Pools
	ReDim $aDP[UBound($aImport) - 1]

	For $dp = 1 To UBound($aImport) - 1
		$aDP[$dp-1] = $aImport[$dp][$dpIndex]
	Next

;~ 	_ArrayDisplay($aDP, 'aDP - Raw')

	$aDP = _ArrayUnique($aDP)
	_ArraySort($aDP)


	_ArrayDelete($aDP, 0)

	If Number($aDP[0]) = Number(UBound($aDP)) Then _ArrayDelete($aDP, 0)

	_ArrayDisplay($aDP, 'aDP - Unique')

	;add device pools to combo box
	For $dp = 0 To UBound($aDP) - 1
		_GUICtrlComboBox_AddString($ComboDevicePool, $aDP[$dp])
	Next


EndFunc

Func _Call($version, $method, $value, $value2 = '', $method_Specific = '', $Desc = '')
	Local	$aXML, $return

	$aXML = _XML_Formatter($version, $method, $value, $value2, $method_Specific, $Desc)

	$return = _SOAP_Open($aXML[0], $aXML[1], $aXML[2], $aXML[3], $aXML[4], $aXML[5])

	Return $return

EndFunc

Func _Call_RIS($version, $method, $value, $status = '', $searchParam = '')
	Local	$aXML, $return

	$aXML = _XML_FormatterRIS($version, $method, $value,$status, $searchParam)
;~ 	_ArrayDisplay($aXML, 'aXML 2')

	$return = _SOAP_Open($aXML[0], $aXML[1], $aXML[2], $aXML[3], $aXML[4], $aXML[5])

	Return $return

EndFunc

Func _Exit()

	GUIDelete($CUCM_Comparison_GUI)
	Exit

EndFunc

Func MyErrFunc()

	Local	$HexNumber
$HexNumber=hex($oMyError.number,8)
Msgbox(0,"COM Test","We intercepted a COM Error !" & @CRLF & @CRLF & _
             "err.description is: " & @TAB & $oMyError.description & @CRLF & _
             "err.windescription:" & @TAB & $oMyError.windescription & @CRLF & _
             "err.number is: " & @TAB & $HexNumber & @CRLF & _
             "err.lastdllerror is: " & @TAB & $oMyError.lastdllerror & @CRLF & _
             "err.scriptline is: " & @TAB & $oMyError.scriptline & @CRLF & _
             "err.source is: " & @TAB & $oMyError.source & @CRLF & _
             "err.helpfile is: " & @TAB & $oMyError.helpfile & @CRLF & _
             "err.helpcontext is: " & @TAB & $oMyError.helpcontext _
            )
SetError(1) ; to check for after this function returns
Endfunc

Func _Test_Fill_List()
	Local	$abogus[21] = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20, 21]

	_Build_Headers($Add_Edit, $aTheHeader)
	_Build_Fields($Add_Edit, $abogus)

;~ 	_Build_Headers($Delete_Edit, $abogus)
;~ 	_Build_Fields($Delete_Edit, $abogus)

EndFunc

Func _Log($verbiage)

	ConsoleWrite($verbiage&@CRLF)
	_GUICtrlStatusBar_SetText($StatusBar, 'Action: ' & $verbiage, 1)

EndFunc







