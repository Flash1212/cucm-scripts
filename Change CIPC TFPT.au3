


;Update CIPC

;~ CUCM 10
;~ 10.23.147.71 = 4793170a
;~ 10.20.7.71 	= 4707140a

;~ CUCM 7
;~ 172.16.10.10 = a0a10ac ; Hex
;~ 172.19.96.10 = a6013ac ; Hex

If ProcessExists('communicatork9.exe') Then ProcessClose('communicatork9.exe')

$Ver = 10


$7KeyValue1 = '0x0a0a10ac'
$7KeyValue2 = '0x0a6013ac'


$10KeyValue1 = '0x4793170a'
$10KeyValue2 = '0x4707140a'

$Key1 = "TftpServer1"
$Key2 = "TftpServer2"

$KeyType = "REG_DWORD"


$x86Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Cisco Systems, Inc.\Communicator"
$x64Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Cisco Systems, Inc.\Communicator"


Switch $ver

	Case 7
		If @OSArch = "X86" Then

			RegWrite($x86Key, $Key1, $KeyType, $7KeyValue1)
			RegWrite($x86Key, $Key2, $KeyType, $7KeyValue2)

		ElseIf @OSArch = "X64" Or "IA64" Then
;~ 			MsgBox(64,'Test', '')
			RegWrite($x64Key, $Key1, $KeyType, $7KeyValue1)
			RegWrite($x64Key, $Key2, $KeyType, $7KeyValue2)


		EndIf

	Case 10
		If @OSArch = "X86" Then

			RegWrite($x86Key, $Key1, $KeyType, $10KeyValue1)
			RegWrite($x86Key, $Key2, $KeyType, $10KeyValue2)

		ElseIf @OSArch = "X64" Or "IA64" Then

			RegWrite($x64Key, $Key1, $KeyType, $10KeyValue1)
			RegWrite($x64Key, $Key2, $KeyType, $10KeyValue2)

		EndIf
EndSwitch




