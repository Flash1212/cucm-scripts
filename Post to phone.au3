#include <Date.au3>
#include <Inet.au3>
#include <Array.au3>
#include <File.au3>
#include <String.au3>
#include <IE.au3>
#include <Array.au3>
#include <WinHTTP.au3>
#include <Base64v2.au3>

Global	$oMyError = ObjEvent("AutoIt.Error", "_COMError") ; Install a custom error handler

$ip = ''

$auth = ""

$Data2  = 'XML=<CiscoIPPhoneExecute>' & _
			'<ExecuteItem Priority="0"URL="Key:Settings"/>'& _
			'</CiscoIPPhoneExecute>'


;~ _Put()
_Put2()

Func _Put2()
	Dim $FSO, $authen, $Mac, $result, $UserAttributes, $objSvrHTTP, $strLine, $strNewContents, $objFSO, $xml, $auth, $Authorization


	$objSvrHTTP = ObjCreate("Msxml2.ServerXMLHTTP.6.0")
	ConsoleWrite("Create Msxml: "&$objSvrHTTP&@CRLF)
	$xml = $Data2
	$WebServer = "http://"&$ip&"/CGI/Execute"
	ConsoleWrite("WebServer: "&$WebServer&@CRLF)
	$open = $objsvrHTTP.open("POST", $WebServer, false)
	ConsoleWrite("Open: "&$open&@CRLF)
	$contType = $objsvrHTTP.setRequestHeader("Content-Type","text/xml")
	ConsoleWrite("Type: "&$contType&@CRLF)
	$creds = $objsvrHTTP.setRequestHeader("Authorization","Basic "&_Base64Encode($auth))
	ConsoleWrite("Credentials: "&$creds.text&@CRLF)
	$length = $objsvrHTTP.setRequestHeader("Content-Length", StringLen($xml))
	ConsoleWrite("Data Length: "&$length&@CRLF)
	$send = $objSvrHTTP.send($xml)
	ConsoleWrite("Sent: "&$send&@CRLF)
	$response = $objSvrHTTP.responseText
	ConsoleWrite("Response: "&$response)
EndFunc



Func _COMError()
	Local	$HexNumber
	$HexNumber = Hex($oMyError.number, 8)
	;MsgBox(16,'Com Error', $HexNumber)
	;FileWriteLine($oLog, $a&',COM error Number is: ' & $HexNumber & ",Windescription is: " & StringTrimRight($oMyError.windescription, 1) &	",Script Line number is: " & $oMyError.scriptline & ",Script Character number is: " & Hex($oMyError.number, 8))
	;FileWriteLine($oLog, $a&',COM error'); Number is: ' & $HexNumber & ",Windescription is: " & StringTrimRight($oMyError.windescription, 1) &	",Script Line number is: " & $oMyError.scriptline & ",Script Character number is: " & Hex($oMyError.number, 8))
	$COMerr = 1
EndFunc